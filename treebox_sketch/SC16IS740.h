#ifndef _SC16IS740_H
#define _SC16IS740_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif


/* Flow control */
enum Flow {
  Disabled = 0,
  RTS,
  CTS,
  RTSCTS,
  XONXOFF
};


class SC16IS740{
  public:
    SC16IS740(uint8_t sselPin);
    
    /**
     * Initialize the board
     * SPI = pin
     * Return true on success, false on failure
     */
    bool init(uint32_t baud, enum Flow flow_ctrl);
    
    /**
     * End the spi2uart and return settings to default
     */
    void end();
    
    /**
     * Transfer data to uart
     * Will block until the len amount of bytes have been transferred, unless
     * connection has been severed, or idle time has surpassed TIMEOUT
     * Return the total number of bytes transferred
     */
    uint16_t print(const uint8_t* tx, const uint16_t len, const uint16_t timeout);
    uint16_t print(const char* tx, const uint16_t timeout);
    uint16_t print(const uint16_t timeout, const char* format, uint16_t max_tx_len, ...);
    
    /**
     * Read data from uart
     * Will block until len bytes have been read or no more bytes to read
     * Return the number of bytes read
     */
    uint16_t read(uint8_t* rx, const uint16_t len);
    
    /**
     * Reset and flush the fifo
     */
    void reset();
    
  private:
    bool spiInitialized = 0;
    uint8_t spiPort = 0;

    /* SC16IS750 Register definitions (shifted to align) */
    enum RegisterName { 
      // 16750 addresses. Registers accessed when LCR[7] = 0.  
      RHR         = 0x00 << 3, /* Rx buffer register     - Read access  */
      THR         = 0x00 << 3, /* Tx holding register    - Write access */
      IER         = 0x01 << 3, /* Interrupt enable reg   - RD/WR access */
     
      // 16750 addresses. Registers accessed when LCR[7] = 1.
      DLL         = 0x00 << 3, /* Divisor latch (LSB)    - RD/WR access */
      DLH         = 0x01 << 3, /* Divisor latch (MSB)    - RD/WR access */
     
      // 16750 addresses. IIR/FCR is accessed when LCR[7:0] <> 0xBF.
      // Bit 5 of the FCR register is accessed when LCR[7] = 1.
      IIR         = 0x02 << 3, /* Interrupt id. register - Read only    */
      FCR         = 0x02 << 3, /* FIFO control register  - Write only   */
    
      // 16750 addresses. EFR is accessed when LCR[7:0] = 0xBF. 
      EFR         = 0x02 << 3, /* Enhanced features reg  - RD/WR access */     
    
      // 16750 addresses.  
      LCR         = 0x03 << 3, /* Line control register  - RD/WR access */
    
      // 16750 addresses. MCR/LSR is accessed when LCR[7:0] <> 0xBF.
      // Bit 7 of the MCR register is accessed when EFR[4] = 1.     
      MCR         = 0x04 << 3, /* Modem control register - RD/WR access */
      LSR         = 0x05 << 3, /* Line status register   - Read only    */
     
      // 16750 addresses. MSR/SPR is accessed when LCR[7:0] <> 0xBF.
      // MSR, SPR register is accessed when EFR[1]=0 and MCR[2]=0. 
      MSR         = 0x06 << 3, /* Modem status register  - Read only    */
      SPR         = 0x07 << 3, /* Scratchpad register    - RD/WR access */
    
      // 16750 addresses. TCR/TLR is accessed when LCR[7:0] <> 0xBF.
      // TCR, TLR register is accessed when EFR[1]=1 and MCR[2]=1.
      TCR         = 0x06 << 3, /* Transmission control register - RD/WR access */
      TLR         = 0x07 << 3, /* Trigger level register        - RD/WR access */
     
      // 16750 addresses. XON, XOFF is accessed when LCR[7:0] = 0xBF.
      XON1        = 0x04 << 3, /* XON1 register          - RD/WR access */
      XON2        = 0x05 << 3, /* XON2 register          - RD/WR access */
      XOFF1       = 0x06 << 3, /* XOFF1 register         - RD/WR access */
      XOFF2       = 0x07 << 3, /* XOFF2 register         - RD/WR access */
     
      // 16750 addresses.
      TXLVL       = 0x08 << 3, /* TX FIFO Level register - Read only    */
      RXLVL       = 0x09 << 3, /* RX FIFO Level register - Read only    */
      IODIR       = 0x0A << 3, /* IO Pin Direction reg   - RD/WR access */
      IOSTATE     = 0x0B << 3, /* IO Pin State reg       - RD/WR access */
      IOINTENA    = 0x0C << 3, /* IO Interrupt Enable    - RD/WR access */
      //        reserved    = 0x0D << 3,
      IOCTRL      = 0x0E << 3, /* IO Control register    - RD/WR access */
      EFCR        = 0x0F << 3, /* Extra features reg     - RD/WR access */
    };
    
    /**
     * Read from a register
     * A register is a 8-bit storage element on the SC16IS750 chip
     * Return the char read
     */
    uint8_t read_register(enum RegisterName registerAddress);
    
    /**
     * Write to a register
     * A register is a 8-bit storage element on the SC16IS750 chip
     */
    void write_register(enum RegisterName registerAddress, uint8_t data);
    
    /**
     * Write a block of data to the THR register
     * Assumes that THR has enough space to write
     */
    void write_block(const uint8_t* data, const uint16_t len);
    
    /**
     * Set up baud rate
     */
    void set_baud(uint32_t baudrate);
    
    /**
     * Set up flow control 
     */
    void set_flow_control(enum Flow type);
    
    /**
     * Set up line control
     */
    void set_line_control(uint8_t parity, uint8_t stopbits, uint8_t numbits);
    
    /**
     * Set FIFO control (first-in first-out) 
     */
    void set_fifo_control(bool enable);
    
    /**
     * Test uart connection
     * Return 1 if connectd, 0 otherwise
     */
    bool connected();
    
    /**
     * Test if there is data to be read on the uart bridge 
     * Return 1 if there is, 0 otherwise
     */
    bool readable();
};

#endif
