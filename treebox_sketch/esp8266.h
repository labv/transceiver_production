#ifndef _esp8266_H
#define _esp8266_H

#define CHUNK_LEN 500

#include "SC16IS740.h"

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class esp8266 {
  public:
    /**
     * activatePin = Arduino output pin used to activate the wifi module
     * debugSpi2Uart = output for debugger display
     */
    esp8266(uint8_t activatePin, SC16IS740 *debugSpi2Uart);
    
    /**
     * Enable or disable the wifi device
     * Enabling it will reset the device and re-initialize
     */
    bool setEnable(bool _enable);
    
    /**
     * Join the router access point
     * Return true on success
     */
    bool joinAccessPoint();

    /**
     * Check for connection status
     * 2: Got IP
     * 3: Connected
     * 4: Disconnected
     */
    bool checkConnection();

    /**
     * Send receipt data to the server
     * receipt_id = either passed in
     * data = receipt data
     */
    bool sendReceipt(char* receipt_id, const uint8_t* tx, uint16_t tx_len);
    
    /**
     * Read the receipt data and store into rx
     * Returns the total length of the receipt. If unsuccessful request, return -1
     * start_index = where to start reading
     * rx_len = number of bytes read
     */
    int16_t readReceipt(char* receipt_id, uint32_t start_index, uint8_t* rx);
    
  private:
    uint8_t activatePin;
    SC16IS740 *debugSpi2Uart;
    
    /**
     * Flush buffer for timeout ms
     */
    void flushBuffer(uint16_t timeout);
    
    /**
     * Wait for response
     * Find successCrit in the response
     */
    bool findFromResponse(const char* successCrit, uint16_t timeout);
    
    /**
     * Extract data from Serial and store them in rx, until either timeout or stopCrit
     * Assume here that rx has the required length
     * Return the number of bytes extracted
     */
    uint16_t extractDataUntil(char* rx, const char* stopCrit, uint16_t timeout);
    
    /**
     * Output the entire response to the debug output
     */
    void outputRespToDebug();
    
    /**
     * Open TCP connection and send the request prep to the chip
     */
    bool openTCP(uint16_t req_len);
};

#endif
