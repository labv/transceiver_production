#include <SPI.h>
#include "spi_nfc.h"
#include <MemoryFree.h>

//#define PN532DEBUG
//#define PN532DEBUG_TTL

// frame definition
#define PN532_PREAMBLE                      (0x00)
#define PN532_STARTCODE1                    (0x00)
#define PN532_STARTCODE2                    (0xFF)
#define PN532_POSTAMBLE                     (0x00)

// normal frame length and position
//#define FRAME_PREAMBLE 0
#define FRAME_START    0
#define FRAME_LEN      2
#define FRAME_TFI      4
#define FRAME_DATA     5

// init target related information
#define NUM_MIFARE_PARAMS 6
#define NUM_FELICA_PARAMS 18
#define NUM_NFCID3T 10

// extended frame
#define XFRAME_PREAMBLE 0
#define XFRAME_START    1
#define XFRAME_NLEN     3
#define XFRAME_NLCS     4
#define XFRAME_LEN1     5
#define XFRAME_LEN2     6
#define XFRAME_TFI      8
#define XFRAME_DATA     9

// APDU lengths
#define CAPDU_LEN      262
#define RAPDU_LEN      258

#define PN532_HOSTTOPN532                   (0xD4)
#define PN532_PN532TOHOST                   (0xD5)

// PN532 Commands
#define PN532_COMMAND_DIAGNOSE              (0x00)
#define PN532_COMMAND_GETFIRMWAREVERSION    (0x02)
#define PN532_COMMAND_GETGENERALSTATUS      (0x04)
#define PN532_COMMAND_READREGISTER          (0x06)
#define PN532_COMMAND_WRITEREGISTER         (0x08)
#define PN532_COMMAND_READGPIO              (0x0C)
#define PN532_COMMAND_WRITEGPIO             (0x0E)
#define PN532_COMMAND_SETSERIALBAUDRATE     (0x10)
#define PN532_COMMAND_SETPARAMETERS         (0x12)
#define PN532_COMMAND_SAMCONFIGURATION      (0x14)
#define PN532_COMMAND_POWERDOWN             (0x16)
#define PN532_COMMAND_RFCONFIGURATION       (0x32)
#define PN532_COMMAND_RFREGULATIONTEST      (0x58)
#define PN532_COMMAND_INJUMPFORDEP          (0x56)
#define PN532_COMMAND_INJUMPFORPSL          (0x46)
#define PN532_COMMAND_INLISTPASSIVETARGET   (0x4A)
#define PN532_COMMAND_INATR                 (0x50)
#define PN532_COMMAND_INPSL                 (0x4E)
#define PN532_COMMAND_INDATAEXCHANGE        (0x40)
#define PN532_COMMAND_INCOMMUNICATETHRU     (0x42)
#define PN532_COMMAND_INDESELECT            (0x44)
#define PN532_COMMAND_INRELEASE             (0x52)
#define PN532_COMMAND_INSELECT              (0x54)
#define PN532_COMMAND_INAUTOPOLL            (0x60)
#define PN532_COMMAND_TGINITASTARGET        (0x8C)
#define PN532_COMMAND_TGSETGENERALBYTES     (0x92)
#define PN532_COMMAND_TGGETDATA             (0x86)
#define PN532_COMMAND_TGSETDATA             (0x8E)
#define PN532_COMMAND_TGSETMETADATA         (0x94)
#define PN532_COMMAND_TGGETINITIATORCOMMAND (0x88)
#define PN532_COMMAND_TGRESPONSETOINITIATOR (0x90)
#define PN532_COMMAND_TGGETTARGETSTATUS     (0x8A)

#define PN532_RESPONSE_INDATAEXCHANGE       (0x41)
#define PN532_RESPONSE_INLISTPASSIVETARGET  (0x4B)

#define PN532_WAKEUP                        (0x55)

#define PN532_SPI_STATREAD                  (0x02)
#define PN532_SPI_DATAWRITE                 (0x01)
#define PN532_SPI_DATAREAD                  (0x03)
#define PN532_SPI_READY                     (0x01)

#define URI_NO_PRE        0x00    // no pre-pending
#define URI_HTTP_WWW      0x01    // http://www.
#define URI_HTTPS_WWW     0x02    // https://www.
#define URI_HTTP          0x03    // http://
#define URI_HTTPS         0x04    // https://

// link to access receipt
// FIXME: modifiable link
static const char receipt_link[] PROGMEM = "gettreebox.com/welcome/receipts?id=";
static const uint16_t receipt_link_len = 35;

// Acknowledgement frame
static byte pn532ack[] = {0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};

// SPI settings
static SPISettings spiSettingNfc(2000000, LSBFIRST, SPI_MODE0);


/**
 * Constructor
 * ss = spi SSEL pin
 * debugSpi2Uart = the SPI to uart debug output object
 */
SpiNfc::SpiNfc(uint8_t ss, SC16IS740 *debugSpi2Uart) {
  this->ss = ss;
  this->debugSpi2Uart = debugSpi2Uart;

  memset(nfcBuffer, 0x00, XFRAME_MAX_LEN);
  nfcBufferLen = 0;
}


SpiNfc::~SpiNfc() {}


/**
 * Configure the PN532 chip
 */
int8_t SpiNfc::Configure(uint16_t timeout){
  int8_t res;
  this->receipt_id = receipt_id;
  
  //- set internal parameters
  // No NAD, no DID, auto ATR_RES, no auto RATS, enable ISO14443-4 PICC, send preamble/postamble
  uint8_t params[] = {PN532_COMMAND_SETPARAMETERS, 0x24};
  //uint8_t params[] = {PN532_COMMAND_GETFIRMWAREVERSION};
  res = writeCommand(params, 2, false, timeout);
  if (res < 0)
    return res;
    
  // get PN532 response
  if (!waitReady(timeout))
    return PN532_TIMEOUT;
  res = readData(nfcBuffer, XFRAME_MAX_LEN);
  if (res < 0)
    return res;
  
  //- set SAM Configuration
  // Normal mode, no timeout, no IRQ
  uint8_t sam[] = {PN532_COMMAND_SAMCONFIGURATION, 0x01, 0x14, 0x01};
  res = writeCommand(sam, 4, false, timeout);
  if (res < 0)
    return res;
    
  // get PN532 response
  if (!waitReady(timeout))
    return PN532_TIMEOUT;
  res = readData(nfcBuffer, XFRAME_MAX_LEN);
  if (res < 0)
    return res;

  return PN532_SUCCESS;
}


/**
 * Initialize the device as an ISO14443a target
 * Return error code on failure
 * Return PN532_SUCCESS on success
 */
int8_t SpiNfc::InitTarget(char* receipt_id, uint16_t timeout) {
  int16_t res;
  this->receipt_id = receipt_id;
  
  //- initialize PN532 as target
  const uint8_t CMD_LEN = 2 + NUM_MIFARE_PARAMS + NUM_FELICA_PARAMS + NUM_NFCID3T + 2;
  uint8_t cmd[CMD_LEN] = {PN532_COMMAND_TGINITASTARGET,
    0x05,              // PICC mode + passive target
    0x04, 0x00,        // MiFare params
    0x12, 0x34, 0x56,
    0x20,
    0,0,0,0,0,0,0,0,   // FeliCaParams
    0,0,0,0,0,0,0,0,
    0,0,
 
    0,0,0,0,0,0,0,0,0,0, // NFCID3t
 
    0, // length of general bytes
    0  // length of historical bytes
  };

  // send command to PN532
  res = writeCommand(cmd, CMD_LEN, false, timeout);
  if (res < 0)
    return res;
    
  // get initiator data
  if (!waitReady(timeout))
    return PN532_TIMEOUT;
  res = readData(nfcBuffer, XFRAME_MAX_LEN);
  if (res < 0)
    return res;
  
  return PN532_SUCCESS;
}


/**
 * Receive bytes and APDU frames
 * Returns PN532_SUCCESS on success
 * Returns error code on failure
 */
int8_t SpiNfc::TargetReceiveBytes(uint16_t timeout){
  int16_t res;
  uint8_t cmd[1] = {PN532_COMMAND_TGGETDATA};
  res = writeCommand(cmd, 1, false, timeout);
  if (res < 0)
    return res;

  if (!waitReady(timeout))
    return PN532_TIMEOUT;
    
  res = readData(nfcBuffer, XFRAME_MAX_LEN);
  if (res < 0)
    return res;
  else {
    // check for the error byte
    if (nfcBuffer[7] == 0x29){
      return PN532_TARGET_RELEASED;
    } else if(nfcBuffer[7] != 0x00){
      return PN532_APDU_ERR;
    } else {
      nfcBufferLen = res;
      return PN532_SUCCESS;
    }
  }
}


/**
 * Send bytes and APDU frames
 * Returns PN532_SUCCESS on success
 * Returns error code on failure
 */
int8_t SpiNfc::TargetSendBytes(uint16_t timeout){
  int16_t res;
  uint8_t cmd[1] = {PN532_COMMAND_TGSETDATA};
  res = writeCommand(cmd, 1, true, timeout);
  if (res < 0)
    return res;

  if (!waitReady(timeout))
    return PN532_TIMEOUT;
  
  res = readData(nfcBuffer, XFRAME_MAX_LEN);
  if (res < 0)
    return res;
  else {
    // check for the error byte
    if (nfcBuffer[7] == 0x29){
      return PN532_TARGET_RELEASED;
    } else if(nfcBuffer[7] != 0x00){
      return PN532_APDU_ERR;
    } else {
      nfcBufferLen = res;
      return PN532_SUCCESS;
    }
  }
}


/**
 * Based on the command received (C-APDU), decipher what response should be sent out
 * The C-APDU should be stored in nfcBuffer. 
 * The appropriate output will be stored in nfcBuffer. The previous content is overwritten
 * 
 * Return PN532_SUCCESS or error code
 */
int8_t SpiNfc::TargetDecipherIO(bool *termination_success){
  // no C-APDU data, nothing to do
  if (nfcBufferLen == 0)
    return PN532_SUCCESS;
    
  if (nfcBufferLen >= 4) {
    // instruction code
    const uint8_t ISO7816_SELECT = 0xA4;
    const uint8_t ISO7816_READ_BINARY = 0xB0;
    // C-APDU offsets
    const uint8_t CAPDU_OFFSET = 8;
    const uint8_t CLA = 0 + CAPDU_OFFSET;
    const uint8_t INS = 1 + CAPDU_OFFSET;
    const uint8_t P1 = 2 + CAPDU_OFFSET;
    const uint8_t P2 = 3 + CAPDU_OFFSET;
    const uint8_t LC = 4 + CAPDU_OFFSET;
    const uint8_t DATA = 5 + CAPDU_OFFSET;

    // request selector by ID
    const uint8_t cc_request[] = { 0xE1, 0x03 };
    const uint8_t ndef_request[] = { 0xE1, 0x04 };    // defined in NDEF capability container

    // request selector by name
    //const uint8_t ndef_tag_application_name_v1[] = { 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x00 };
    const uint8_t ndef_tag_application_name_v2[] = { 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01 };

    /* used for CC file (NDEF capability container) */
    const uint8_t nfcforum_capability_container[] = {
      0x00, 0x0F, /* CCLEN 15 bytes */
      0x20,       /* Mapping version 2.0, use option -1 to force v1.0 */
      0x00, 0x54, /* MLe Maximum R-ADPU data size */
      0x00, 0xFF, /* MLc Maximum C-ADPU data size */
      0x04,       /* T field of the NDEF File-Control TLV */
      0x06,       /* L field of the NDEF File-Control TLV */
      /* V field of the NDEF File-Control TLV */
      0xE1, 0x04, /* File identifier */
      0xFF, 0xFE, /* Maximum NDEF Size */
      0x00,       /* NDEF file read access condition */
      0xFF,       /* NDEF file write access condition */
    };
    const size_t MAX_NDEF_LEN = 0xFFFE;    // this limit was defined in the NDEF capability container

    // C-APDU must begin with 0x00
    if (nfcBuffer[CLA] != 0x00)
      return ISO7816_ENOTSUP;

    // expected page and length from the C-APDU of the R-APDU data
    uint16_t offset = (nfcBuffer[P1] << 8) + nfcBuffer[P2];
    uint16_t exp_len = nfcBuffer[LC];

    // temp storage for buildUriNdef response
    int8_t res;
      
    // decipher the instruction that came from the initiator
    switch (nfcBuffer[INS]) {
      case ISO7816_SELECT:
        // select is used to determine the capability of the target as well as to establish
        // uniform data exchange formats

        //- select begins
        switch (nfcBuffer[P1]) {
          case 0x00: /* Select by ID */
#ifdef PN532DEBUG
            debugSpi2Uart->print("nfc select id\n", 0);
#endif
#ifdef PN532DEBUG_TTL
            Serial.println(F("nfc select id"));
#endif
            if ((nfcBuffer[P2] | 0x0C) != 0x0C){
              return ISO7816_ENOTSUP;
            }

            // determine which file identifier has been requested
            if ((nfcBuffer[LC] == sizeof(cc_request)) && (0 == memcmp(cc_request, nfcBuffer + DATA, nfcBuffer[LC]))) {
              // capability container requested
              memcpy(nfcBuffer, "\x90\x00", nfcBufferLen = 2);
              nfc_state = CC_FILE;
            } else if ((nfcBuffer[LC] == sizeof(ndef_request)) && (0 == memcmp(ndef_request, nfcBuffer + DATA, nfcBuffer[LC]))) {
              // NDEF encapsulated data requested
              memcpy(nfcBuffer, "\x90\x00", nfcBufferLen = 2);
              nfc_state = NDEF_FILE;
            } else {
              // unknown data type requested, return "no info given"
              memcpy(nfcBuffer, "\x6a\x00", nfcBufferLen = 2);
              nfc_state = NONE;
            }

            break;
            
          case 0x04: /* Select by name */
#ifdef PN532DEBUG
            debugSpi2Uart->print("nfc select name\n", 0);
#endif
#ifdef PN532DEBUG_TTL
            Serial.println(F("nfc select name"));
#endif
            if (nfcBuffer[P2] != 0x00)
              return ISO7816_ENOTSUP;

            if ((nfcBuffer[LC] == sizeof(ndef_tag_application_name_v2)) && (0 == memcmp(ndef_tag_application_name_v2, nfcBuffer + DATA, nfcBuffer[LC]))) {
              memcpy(nfcBuffer, "\x90\x00", nfcBufferLen = 2);
            } else {
              // unknown tag application, return "file not found"
              memcpy(nfcBuffer, "\x6a\x82", nfcBufferLen = 2);
            }

            break;
            
          default:
            return ISO7816_ENOTSUP;
        }
        
#ifdef PN532DEBUG
        debugSpi2Uart->print(0, "nfc state = %d\n", 15, nfc_state);
#endif
#ifdef PN532DEBUG_TTL
        Serial.print(F("nfc state = "));
        Serial.print(nfc_state, HEX);
        Serial.println();
#endif
        break;
        //- select ends
        
      case ISO7816_READ_BINARY:
        // read requests the data to be returned from the target
        //- read begins

        // check for overflow
        if ((size_t)(nfcBuffer[LC] + 2) > RAPDU_LEN) {
          return ISO7816_OVERFLOW;
        }

        // from previous request, an NFC payload type was selected
        switch (nfc_state) {
          case NONE:
            // we have yet to receive request as to what data to send out
            memcpy(nfcBuffer, "\x6a\x82", nfcBufferLen = 2);
            break;
          case CC_FILE:
            // request capability container to be sent out
            // P1P2 defines the offset
#ifdef PN532DEBUG
            debugSpi2Uart->print("nfc read cc\n", 0);
#endif
#ifdef PN532DEBUG_TTL
            Serial.println(F("nfc read cc"));
#endif

            // prepare tx
            memcpy(nfcBuffer, nfcforum_capability_container + offset, exp_len);
            memcpy(nfcBuffer + exp_len, "\x90\x00", 2);
            nfcBufferLen = exp_len + 2;
            
            break;
            
          case NDEF_FILE:
            // initiator requested NDEF data to be returned
#ifdef PN532DEBUG
            debugSpi2Uart->print("nfc read ndef\n", 0);
#endif
#ifdef PN532DEBUG_TTL
            Serial.println(F("nfc read ndef"));
#endif
          
            // encapsulate payload in NDEF
            res = buildUriNdef(offset, exp_len, nfcBuffer);
            if (res < 0)
              return NDEF_ERR;
            else if (res == 1)
              *termination_success = true;

            memcpy(nfcBuffer + exp_len, "\x90\x00", 2);
            nfcBufferLen = exp_len + 2;

            break;
        }
        
        break;
        //- read ends

      default: // Unknown
        return ISO7816_ENOTSUP;
    }
  } else {
    return ISO7816_ENOTSUP;
  }

  return PN532_SUCCESS;
}


/**
 * Read the ACK frame
 * Return true if ACK frame is received
 */
bool SpiNfc::readAck() {
  uint8_t ackbuff[6];

  if (readData(ackbuff, 6) < 0)
    return false;

  return (0 == strncmp((char *)ackbuff, (char *)pn532ack, 6));
}


/**
 * Check for isReady() until timeout
 * timeout = 0: block forever until true
 */
bool SpiNfc::waitReady(uint16_t timeout) {
  uint16_t timer = 0;
  while (!isReady()) {
    if (timeout != 0) {
      timer += 1;
      if (timer > timeout) {
        return false;
      }
    }
    delay(1);
  }
  return true;
}


/**
 * Check if PN532 is ready for data reading
 */
bool SpiNfc::isReady() {
  // SPI read status and check if ready.
  SPI.beginTransaction(spiSettingNfc);
  digitalWrite(ss, LOW);
  //delay(2);
  SPI.transfer(PN532_SPI_STATREAD);
  // read byte
  uint8_t x = SPI.transfer(0x00);

  digitalWrite(ss, HIGH);
  SPI.endTransaction();

  // Check if status is ready.
  return x == PN532_SPI_READY;
}


/**
 * Read data from PN532
 * Assumes that data are ready to be read
 * Return the number of bytes received or error code
 */
int16_t SpiNfc::readData(uint8_t* buff, uint16_t n) {
#ifdef PN532DEBUG
  debugSpi2Uart->print("Reading: ", 0);
#endif
#ifdef PN532DEBUG_TTL
  Serial.print(F("Reading: "));
#endif

  // begin SPI transaction
  SPI.beginTransaction(spiSettingNfc);
  digitalWrite(ss, LOW);

  // SPI write command and data
  SPI.transfer(PN532_SPI_DATAREAD);
  for (uint16_t i = 0; i < n; i++) {
    buff[i] = SPI.transfer(0x00);
  }

  // end SPI transaction
  digitalWrite(ss, HIGH);
  SPI.endTransaction();
  
#ifdef PN532DEBUG
  debugOutputFrame(buff, n);
  debugSpi2Uart->print("\n", 0);
#endif
#ifdef PN532DEBUG_TTL
  debugOutputFrame(buff, n);
  Serial.println();
#endif

  return countPacketLength(buff, n);
}


/**
 * Write command frame to PN532
 * Automatically appends the preamble, postamble and checksum
 * Return true if acknowledge received
 */
int8_t SpiNfc::writeCommand(uint8_t* cmd, uint8_t cmdlen, bool sendNfcBuffer, uint16_t timeout) {
  // SPI command write.
  uint8_t checksum;
  uint16_t actCmdLen = 1 + cmdlen + (sendNfcBuffer ? nfcBufferLen : 0);

#ifdef PN532DEBUG
  debugSpi2Uart->print("\nSending: ", 0);
  
  debugSpi2Uart->print(0, " 0x%x", 6, PN532_PREAMBLE);
  debugSpi2Uart->print(0, " 0x%x", 6, PN532_STARTCODE1);
  debugSpi2Uart->print(0, " 0x%x", 6, PN532_STARTCODE2);
  debugSpi2Uart->print(0, " 0x%x", 6, actCmdLen);
  debugSpi2Uart->print(0, " 0x%x", 6, (byte)(~actCmdLen + 1));
  debugSpi2Uart->print(0, " 0x%x", 6, PN532_HOSTTOPN532);
#endif
#ifdef PN532DEBUG_TTL
  Serial.print(F("Sending: "));

  Serial.print(F(" 0x")); Serial.print(PN532_PREAMBLE, HEX);
  Serial.print(F(" 0x")); Serial.print(PN532_STARTCODE1, HEX);
  Serial.print(F(" 0x")); Serial.print(PN532_STARTCODE2, HEX);
  Serial.print(F(" 0x")); Serial.print(actCmdLen, HEX);
  Serial.print(F(" 0x")); Serial.print((byte)(~actCmdLen + 1), HEX);
  Serial.print(F(" 0x")); Serial.print(PN532_HOSTTOPN532, HEX);
#endif

  // begin SPI transaction
  SPI.beginTransaction(spiSettingNfc);
  digitalWrite(ss, LOW);
  delay(2);   // this is needed to wake PN532 up if in power down mode

  SPI.transfer(PN532_SPI_DATAWRITE);

  checksum = PN532_PREAMBLE + PN532_STARTCODE1 + PN532_STARTCODE2;
  SPI.transfer(PN532_PREAMBLE);
  SPI.transfer(PN532_STARTCODE1);
  SPI.transfer(PN532_STARTCODE2);

  SPI.transfer(actCmdLen);
  SPI.transfer((byte)(~actCmdLen + 1));

  SPI.transfer(PN532_HOSTTOPN532);
  checksum += PN532_HOSTTOPN532;

  // send cmd
  for (uint8_t i = 0; i < cmdlen; i++) {
    SPI.transfer(cmd[i]);
    checksum += cmd[i];
  }

  // send nfcBuffer?
  if (sendNfcBuffer){
    for (uint16_t i = 0; i < nfcBufferLen; i++){
      SPI.transfer(nfcBuffer[i]);
      checksum += nfcBuffer[i];
    }
  }

  SPI.transfer(~checksum);
  SPI.transfer(PN532_POSTAMBLE);

  digitalWrite(ss, HIGH);
  SPI.endTransaction();

#ifdef PN532DEBUG
  for (uint8_t i=0; i<cmdlen; i++){
    debugSpi2Uart->print(0, " 0x%x", 6, cmd[i]);
  }

  if (sendNfcBuffer){
    for (uint16_t i = 0; i < nfcBufferLen; i++){
      debugSpi2Uart->print(0, " 0x%x", 6, nfcBuffer[i]);
    }
  }

  debugSpi2Uart->print(0, " 0x%x", 6, (byte)(~checksum));
  debugSpi2Uart->print(0, " 0x%x", 6, PN532_POSTAMBLE);
  debugSpi2Uart->print("\n", 0);
#endif
#ifdef PN532DEBUG_TTL
  for (uint8_t i=0; i<cmdlen; i++){
    Serial.print(F(" 0x")); Serial.print(cmd[i], HEX);
  }

  if (sendNfcBuffer){
    for (uint16_t i = 0; i < nfcBufferLen; i++){
      Serial.print(F(" 0x")); Serial.print(nfcBuffer[i], HEX);
    }
  }

  Serial.print(F(" 0x")); Serial.print((byte)(~checksum), HEX);
  Serial.print(F(" 0x")); Serial.print(PN532_POSTAMBLE, HEX);
  Serial.println();
#endif

  // wait for acknowledgement
  if (!waitReady(timeout))
    return PN532_TIMEOUT;
  if (!readAck())
    return PN532_NO_ACK;

  return PN532_SUCCESS;
}


/**
 * Return the length of the packet received from buff, including preamble and postamble
 * Or on error, return error code
 */
int16_t SpiNfc::countPacketLength(uint8_t* frame, uint16_t len){
  uint16_t packetLen = 0;
  uint8_t TFI_index = 0;
  uint8_t check = 0x00;
  
  // check preamble
  if (frame[0] != 0x00 || frame[1] != 0x00 || frame[2] != 0xFF)
    return PN532_INVALID_FRAME;
  
  if (frame[3] == 0xFF && frame[4] == 0xFF){
    // extended information frame
    // 00 00 FF FF FF LENM LENL LCS TFI PD0 ... PDN DCS 00

    // check length checksum
    if ((uint8_t)(frame[5]+frame[6]+frame[7]) != 0x00)
      return PN532_INVALID_FRAME;

    TFI_index = 8;
    packetLen = 8 + frame[5]*256 + frame[6] + 2;
    
  } else if (frame[3] == 0x00 && frame[4] == 0xFF){
    // ack frame
    // 00 00 FF 00 FF 00

    return 6;
    
  } else {
    // normal information frame
    // 00 00 FF LEN LCS TFI PD0 ... PDN DCS 00

    // check length checksum
    if ((uint8_t)(frame[3]+frame[4]) != 0x00)
      return PN532_INVALID_FRAME;

    // check if this is an error frame
    if (frame[5] == 0x7F && frame[6] == 0x81)
      return PN532_APP_ERR;

    TFI_index = 5;
    packetLen = 5 + frame[3] + 2;
  }

  // check pakcet data checksum
  for (uint16_t i=TFI_index; i<packetLen-1; i++){
    check += frame[i];
  }
  if (check != 0)
    return PN532_INVALID_FRAME;
  else
    return packetLen;
}


/**
 * Build the URL for receipt retrieval using receipt_id
 * offset = offset from C-APDU
 * LC = expected NFC buffer length of the R-APDU data
 * Output to tx
 * 
 * Return 0 for successful output
 * Return 1 if tx contains the end of the ndef
 * Return -1 for failure
 */
int8_t SpiNfc::buildUriNdef(uint16_t offset, uint8_t LC, uint8_t* tx){
  // FIXME: increase size as required if link size increases
  const uint8_t ndef_size = 70; uint8_t ndef[ndef_size]; 
  ndef[0] = 0;
  
  // length of records:
  // 1 byte for TNF + 1 byte for type len + 1 byte for payload length + 1 byte for type ('U') + 
  // 1 byte for URI prepend + x bytes for payload
  uint8_t receipt_id_len = strlen(receipt_id);
  ndef[1] = 4 + 1 + receipt_link_len + receipt_id_len;
  uint8_t act_ndef_size = ndef[1] + 2;
  // check that we don't have overflow
  if (act_ndef_size > ndef_size)
    return -1;
  // check for integrity of request
  if (offset+LC > act_ndef_size)
    return -1;
  
  // well-known TNF (0x01) | is_begin (0x80) | is_end (0x40) | is_short (0x10)
  ndef[2] = 0x01 | 0x80 | 0x40 | 0x10;    
  
  // type length
  ndef[3] = 1;
  
  // payload length:
  // 1 byte for URI prepend + x bytes for base link + x bytes for receipt_id
  ndef[4] = 1 + receipt_link_len + receipt_id_len;
  
  // type (URI)
  ndef[5] = 'U';
  
  // payload
  // FIXME: modify pre-pend as required
  ndef[6] = URI_HTTP;
  for (uint8_t i=0; i<receipt_link_len; i++){
    ndef[7+i] = pgm_read_byte_near(receipt_link+i);
  }
  memcpy(ndef+7+receipt_link_len, receipt_id, receipt_id_len);
  
  // copy to tx
  memcpy(tx, ndef+offset, LC);

  if (offset+LC == act_ndef_size)
    return 1;
  else
    return 0;
}


/**
 * Output the information frame to the debug output
 */
void SpiNfc::debugOutputFrame(uint8_t* frame, uint16_t len){
  // find out what type of frame it is
  int16_t packet_len = countPacketLength(frame, len);
  if (packet_len < 0) return;
  
  for (uint16_t i=0; i<packet_len; i++){
#ifdef PN532DEBUG
    debugSpi2Uart->print(0, " 0x%x", 6, frame[i]);
#endif
#ifdef PN532DEBUG_TTL
    Serial.print(F(" 0x")); Serial.print(frame[i], HEX);
#endif
  }
}

