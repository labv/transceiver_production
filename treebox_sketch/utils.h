#ifndef _UTILS_H
#define _UTILS_H

#define LARGEST_ULONG 4294967295

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class Utilities{
  public:
    /**
     * Return the elapsed time since start 
     */
    static uint32_t getElapsedTime(uint32_t start, uint32_t now);
    
    /**
     * Convert the 128bit uuid from bytes to string
     * Assume uuid_str is 17 bytes long (last byte for nul)
     */
    static void uuidToString(const uint8_t* uuid, char* uuid_str);
};

#endif
