#ifndef _SPI_NFC
#define _SPI_NFC

#include "SC16IS740.h"

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#define XFRAME_MAX_LEN  275

// error codes
#define PN532_SUCCESS                 (0)
#define PN532_NO_ACK                  (-1)
#define PN532_TIMEOUT                 (-2)
#define PN532_INVALID_FRAME           (-3)
#define PN532_NO_SPACE                (-4)
#define PN532_APP_ERR                 (-5)
#define PN532_APDU_ERR                (-6)
#define PN532_TARGET_RELEASED         (-7)
#define ISO7816_ENOTSUP               (-8)
#define ISO7816_OVERFLOW              (-9)
#define NDEF_ERR                      (-10)


class SpiNfc{
  public:
    /**
     * Constructor
     * ss = spi SSEL pin
     * debugSpi2Uart = the SPI to uart debug output object
     */
    SpiNfc(uint8_t ss, SC16IS740 *debugSpi2Uart);
    ~SpiNfc();

    // Buffer used to store the nfc i/o
    uint8_t nfcBuffer[XFRAME_MAX_LEN];
    uint16_t nfcBufferLen;
    // Receipt id to be sent to the Android app
    char* receipt_id;

    /**
     * Configure the PN532 chip
     */
    int8_t Configure(uint16_t timeout);
    
    /**
     * Initialize the device as an ISO14443a target
     * Return error code on failure
     * Return PN532_SUCCESS on success
     */
    int8_t InitTarget(char* receipt_id, uint16_t timeout);
    
    /**
     * Receive bytes and APDU frames
     * Returns PN532_SUCCESS on success
     * Returns error code on failure
     */
    int8_t TargetReceiveBytes(uint16_t timeout);
    
    /**
     * Send bytes and APDU frames
     * Returns PN532_SUCCESS on success
     * Returns error code on failure
     */
    int8_t TargetSendBytes(uint16_t timeout);

    /**
     * Based on the command received (C-APDU), decipher what response should be sent out
     * The C-APDU should be stored in nfcBuffer. 
     * The appropriate output will be stored in nfcBuffer. The previous content is overwritten
     * 
     * Return PN532_SUCCESS or error code
     */
    int8_t TargetDecipherIO(bool *termination_success);

  private:
    // pin for SPI select
    uint8_t ss;
    // debug serial output
    SC16IS740 *debugSpi2Uart;

    /* NFC payload file types
     * CC = capability container */
    enum nfc_payload_type 
    {
      NONE, 
      CC_FILE, 
      NDEF_FILE 
    } nfc_state;

    /**
     * Write command frame to PN532
     * Automatically appends the preamble, postamble and checksum
     * Return true if acknowledge received
     */
    int8_t writeCommand(uint8_t* cmd, uint8_t cmdlen, bool sendNfcBuffer, uint16_t timeout);

    /**
     * Read data from PN532
     * Assumes that data are ready to be read
     * Return the number of bytes received or error code
     */
    int16_t readData(uint8_t* buff, uint16_t n);

    /**
     * Return the length of the packet received from buff, including preamble and postamble
     */
    int16_t countPacketLength(uint8_t* buff, uint16_t n);
    
    /**
     * Check if PN532 is ready for data reading
     */
    bool isReady();
    
    /**
     * Check for isReady() until timeout
     * timeout = 0: block forever until true
     */
    bool waitReady(uint16_t timeout);
    
    /**
     * Read the ACK frame
     * Return true if ACK frame is received
     */
    bool readAck();

    /**
     * Output the information frame to the debug output
     */
    void debugOutputFrame(uint8_t* frame, uint16_t len);

    /**
     * Build the URL for receipt retrieval using receipt_id
     * offset = offset from C-APDU
     * LC = expected NFC buffer length of the R-APDU data
     * Output to tx
     * 
     * Return 0 for successful output
     * Return 1 if tx contains the end of the ndef
     * Return -1 for failure
     */
    int8_t buildUriNdef(uint16_t offset, uint8_t LC, uint8_t* tx);
};

#endif
