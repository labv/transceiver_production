#include <avr/pgmspace.h>
#include <MemoryFree.h>
#include <SPI.h>

#include "SC16IS740.h"
#include "spi_nfc.h"
#include "esp8266.h"
#include "escpos.h"
#include "utils.h"

#define BUZZER_OUT   2
#define BYPASS_IN    3
#define PWR_LED_OUT  4
#define TAP_LED_OUT  5
#define WIFI_ENABLE  6
#define PRINTER_SSEL 7
#define PC_SSEL      8
#define PN532_SSEL   9
#define DEBUG_SSEL   10    // SPI SSEL pin for debug serial output

#define STD_TIMEOUT  500
#define TAP_TIMEOUT  20000


// machine state
enum STATE {
  STANDBY,
  RECEIPT_NFC_UPLOAD,
  RECEIPT_PRINT_UPLOAD,
  ERROR
} machine_state;

// for NFC
SpiNfc *spiNfc;

// Wifi module
esp8266 *wifi;

// debug spi2uart output
SC16IS740 *debugSpi2Uart;

// bi-directional POS/printer communication
escpos *posComm;

// receipt chunk
// for storing receipts
#define RECEIPT_CHUNK_MAX_SIZE 500
uint8_t receipt_chunk[RECEIPT_CHUNK_MAX_SIZE];
int16_t receipt_chunk_len;
// receipt id
#define RECEIPT_ID_LEN 26
char receipt_id_str[RECEIPT_ID_LEN];


/**
 * Reset receipt chunk
 */
void clear_receipt(){
  receipt_chunk_len = 0;
  memset(receipt_chunk, 0x00, RECEIPT_CHUNK_MAX_SIZE);
}


void setup() {
  // setup IO pins
  pinMode(BUZZER_OUT, OUTPUT);
  pinMode(BYPASS_IN, INPUT);
  pinMode(PWR_LED_OUT, OUTPUT);
  pinMode(TAP_LED_OUT, OUTPUT);
  pinMode(WIFI_ENABLE, OUTPUT);

  // set up SPI circuit
  SPI.begin();
  pinMode(PN532_SSEL, OUTPUT);
  pinMode(DEBUG_SSEL, OUTPUT);
  digitalWrite (PN532_SSEL, HIGH);
  digitalWrite (DEBUG_SSEL, HIGH);
  digitalWrite (PC_SSEL, HIGH);

  // begin uart
  Serial.begin(115200);

  // debug spi2uart
  debugSpi2Uart = new SC16IS740(DEBUG_SSEL);
  if (!debugSpi2Uart->init(115200, Disabled)) {
    machine_state = ERROR;
    goto END_OF_SETUP;
  }

  // initialize POS/printer communication
  posComm = new escpos(debugSpi2Uart, receipt_chunk, RECEIPT_CHUNK_MAX_SIZE);
  if (!posComm->init(PRINTER_SSEL, PC_SSEL)){
    debugSpi2Uart->print("\n\nposComm init fail\n", 0);
    machine_state = ERROR;
    goto END_OF_SETUP;
  }

  // NFC
  spiNfc = new SpiNfc(PN532_SSEL, debugSpi2Uart);
  if (spiNfc->Configure(100) != PN532_SUCCESS) {
    debugSpi2Uart->print("\n\nNFC init fail\n", 0);
    machine_state = ERROR;
    goto END_OF_SETUP;
  }

  // Wifi module
  wifi = new esp8266(WIFI_ENABLE, debugSpi2Uart);
  if (!wifi->setEnable(true) || !wifi->joinAccessPoint() || !wifi->checkConnection()){
    machine_state = ERROR;
    goto END_OF_SETUP;
  }

  // initialize machine state
  machine_state = STANDBY;

  debugSpi2Uart->print("\n\nInit success\n", 0);


  END_OF_SETUP:
  return;
}


void loop() {
  if (machine_state == STANDBY){
    // Standby mode
    debugSpi2Uart->print("\nStandby Mode\n", 0);

    // reset buffer and receipt id
    clear_receipt();
    memset(receipt_id_str, '\0', RECEIPT_ID_LEN);
    
    // clear POS communication state
    posComm->pc_comm_reset();

    // turn on power LED and ready LED
    digitalWrite(PWR_LED_OUT, HIGH);
    digitalWrite(TAP_LED_OUT, HIGH);

    receipt_chunk_len = posComm->pc_communicate();
    if (receipt_chunk_len < 0){
      machine_state = ERROR;
      goto END_LOOP;
    } else if (receipt_chunk_len > 0){
      if (digitalRead(BYPASS_IN) == LOW)
        machine_state = RECEIPT_NFC_UPLOAD;
      else
        machine_state = RECEIPT_PRINT_UPLOAD;
    }

    return;
    
  } else if (machine_state == RECEIPT_NFC_UPLOAD){
    // Upload receipt to server, then begin NFC
    debugSpi2Uart->print("\nNFC Upload Mode\n", 0);

    while (receipt_chunk_len > 0){
      // check for bypass switch
      if (digitalRead(BYPASS_IN) == HIGH){
        machine_state = RECEIPT_PRINT_UPLOAD;
        goto END_LOOP;
      }

      // upload to server
      if (!wifi->sendReceipt(receipt_id_str, receipt_chunk, RECEIPT_CHUNK_MAX_SIZE)){
        machine_state = ERROR;
        goto END_LOOP;
      }

      // get new data
      receipt_chunk_len = posComm->pc_communicate();
      if (receipt_chunk_len < 0){
        machine_state = ERROR;
        goto END_LOOP;
      }
    }

    // do NFC
    bool blink_status = true;
    uint32_t start = millis();
    uint32_t now = millis();
    // this loop times out if user does not tap the phone with the device
    do {
      // check for bypass switch
      if (digitalRead(BYPASS_IN) == HIGH){
        machine_state = RECEIPT_PRINT_UPLOAD;
        goto END_LOOP;
      }

      // blink
      if (blink_status){
        digitalWrite(TAP_LED_OUT, LOW);
        blink_status = false;
      } else {
        digitalWrite(TAP_LED_OUT, HIGH);
        blink_status = true;
      }

      // wait for initiator (Android)
      int8_t res = spiNfc->InitTarget(receipt_id_str, STD_TIMEOUT);
      bool termination_success = false;
      if (res == PN532_SUCCESS) {
        // fast loop NFC communication
        while (true){
          res = spiNfc->TargetReceiveBytes(0);
  
          if (res == PN532_SUCCESS) {
            res = spiNfc->TargetDecipherIO(&termination_success);
  
            if (res == PN532_SUCCESS) {
              res = spiNfc->TargetSendBytes(0);
  
              if (res != PN532_SUCCESS && res != PN532_TIMEOUT) {
                // fail
                debugSpi2Uart->print("NFC send err\n", 0);
                break;  // break the fast loop
              }
            } else {
              // fail
              debugSpi2Uart->print("NFC IO err\n", 0);
              break;  // break the fast loop
            }
          } else if (res != PN532_TIMEOUT) {
            // fail
            debugSpi2Uart->print("NFC read err\n", 0);
            break;  // break the fast loop
          }
  
          if (termination_success) {
            // NFC communication success, we're done here
            debugSpi2Uart->print("NFC success\n", 0);
  
            tone (BUZZER_OUT, 50, 200);

            // take us back to the standby mode
            machine_state = STANDBY;
            goto END_LOOP;
          }
        }
      }

      // update timer
      now = millis();
    } while (Utilities::getElapsedTime(start,now) < TAP_TIMEOUT);

    return;
    
  } else if (machine_state == RECEIPT_PRINT_UPLOAD){
    // Upload receipt to server, while printing it
    debugSpi2Uart->print("\nPrint Upload Mode\n", 0);

    // turn NFC LED off
    digitalWrite(TAP_LED_OUT, LOW);

    // initialize printer
    if (!posComm->printer_status(STD_TIMEOUT)){
      machine_state = ERROR;
      goto END_LOOP;
    }

    // We have previously received something from the POS
    if (receipt_chunk_len > 0){
      // This means that we have not yet communicated with the server. This is the first chunk
      // Pass to printer directly
      bool is_start = (strlen(receipt_id_str) == 0);
      if (is_start && !posComm->printer_transfer(receipt_chunk, receipt_chunk_len, STD_TIMEOUT)){
        machine_state = ERROR;
        goto END_LOOP;
      }

      // store the last chunk to server
      if (!wifi->sendReceipt(receipt_id_str, receipt_chunk, RECEIPT_CHUNK_MAX_SIZE)){
        machine_state = ERROR;
        goto END_LOOP;
      }

      // if we're not at the beginning, then we need to retrieve everything from the server and send to the printer
      if (!is_start){
        uint32_t index = 0;

        // The last possible chunk would have length <= RECEIPT_CHUNK_MAX_SIZE
        do {
          receipt_chunk_len = wifi->readReceipt(receipt_id_str, index, receipt_chunk);
          if (receipt_chunk_len < 0){
            machine_state = ERROR;
            goto END_LOOP;
          }

          // pass to printer
          if (!posComm->printer_transfer(receipt_chunk, receipt_chunk_len, STD_TIMEOUT)){
            machine_state = ERROR;
            goto END_LOOP;
          }
          
          index += receipt_chunk_len;
        } while(receipt_chunk_len == RECEIPT_CHUNK_MAX_SIZE);
      }

      // get fresh data from the POS PC
      receipt_chunk_len = posComm->pc_communicate();
      if (receipt_chunk_len < 0){
        machine_state = ERROR;
        goto END_LOOP;
      }
    }

    while (receipt_chunk_len > 0){
      // send to printer
      if (!posComm->printer_transfer(receipt_chunk, receipt_chunk_len, STD_TIMEOUT)){
        machine_state = ERROR;
        goto END_LOOP;
      }

      // get fresh data from the POS PC
      receipt_chunk_len = posComm->pc_communicate();
      if (receipt_chunk_len < 0){
        machine_state = ERROR;
        goto END_LOOP;
      }
    }

    machine_state = STANDBY;
    return;
    
  } else {
    // ERROR state
    debugSpi2Uart->print("ERROR STATE\n", 0);

    // turn NFC LED off
    digitalWrite(TAP_LED_OUT, LOW);

    // blink error light
    // block forever
    while (1){
      digitalWrite(PWR_LED_OUT, LOW);
      delay(1000);
      digitalWrite(PWR_LED_OUT, HIGH);
      delay(1000);
    }
  }

  END_LOOP:
  return;
}
