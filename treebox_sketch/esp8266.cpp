#include <avr/pgmspace.h>
//#include <MemoryFree.h>
#include "esp8266.h"
#include "utils.h"

// request heads
static const char http_dir[] PROGMEM = "/transceiver/receipts/";
static const uint16_t http_dir_len = 22;
static const char http_dir_sec[] PROGMEM = "/receipt_content";
static const uint16_t http_dir_sec_len = 16;
static const char http_host[] PROGMEM = " HTTP/1.1\r\nHost: tapi.dev.gettreebox.com:80\r\n";
static const uint16_t http_host_len = 45;
static const char http_auth[] PROGMEM = "Authorization: TransceiverAuth ";
static const uint16_t http_auth_len = 31;
static const char http_content_len[] PROGMEM = "Content-Length: ";
static const uint16_t http_content_len_len = 16;
static const char http_content_type[] PROGMEM = "Content-Type: multipart/form-data; boundary=";
static const uint16_t http_content_type_len = 44;
static const char http_boundary[] PROGMEM = "----5h2o9m1e";
static const uint16_t http_boundary_len = 12;
static const char content_disposition_user_id[] PROGMEM = "Content-Disposition: form-data; name=\"user_id\"\r\n";
static const uint16_t content_disposition_user_id_len = 48;
static const char content_disposition_retailer_id[] PROGMEM = "Content-Disposition: form-data; name=\"retailer_id\"\r\n";
static const uint16_t content_disposition_retailer_id_len = 52;
static const char content_disposition_receipt_client_id[] PROGMEM = "Content-Disposition: form-data; name=\"receipt_client_id\"\r\n";
static const uint16_t content_disposition_receipt_client_id_len = 58;
static const char content_disposition_receipt_content[] PROGMEM = "Content-Disposition: form-data; name=\"receipt_content\"\r\n";
static const uint16_t content_disposition_receipt_content_len = 56;

// FIXME: each device would have a unique ID
static const char device_uid[] PROGMEM = "uid=55cc8f0b646576627e010000, ";
static const uint16_t device_uid_len = 30;


esp8266::esp8266(uint8_t activatePin, SC16IS740 *debugSpi2Uart){
  this->activatePin = activatePin;
  this->debugSpi2Uart = debugSpi2Uart;
  
  // flush Serial buffer
  flushBuffer(500);
}


/**
 * Enable or disable the wifi device
 * Enabling it will reset the device and re-initialize
 */
bool esp8266::setEnable(bool _enable){
  bool success = true;
  
  // activate the chip
  digitalWrite(activatePin, _enable);
  
  if (_enable){
    // reset device
    Serial.println(F("AT+RST"));
    if (!findFromResponse("ready", 2000)) {
      debugSpi2Uart->print("Wifi rst fail\n", 0);
      success = false;
    }

    // flush Serial buffer
    flushBuffer(50);
  }
  
  return success;
}


/**
 * Join the router access point
 * Return true on success
 */
bool esp8266::joinAccessPoint(){
  bool success = true;
  
  // connect to Wifi AP
  // FIXME: store dependent
  Serial.println(F("AT+CWJAP=\"luj\",\"UC0nThree\""));
  if (!findFromResponse("WIFI GOT IP", 17000)){
    debugSpi2Uart->print("AP fail\n", 0);
    success = false;
  }
  findFromResponse("OK", 2000);

  // flush Serial buffer
  flushBuffer(100);
  
  return success;
}


/**
 * Check for connection status
 * 2: Got IP
 * 3: Connected
 * 4: Disconnected
 */
bool esp8266::checkConnection(){
  bool success = true;
  
  // connect to server
  Serial.println(F("AT+CIPSTATUS"));
  if (!findFromResponse("STATUS:2", 100)){
    debugSpi2Uart->print("Bad wifi conn\n", 0);
    success = false;
  }
  findFromResponse("OK", 50);

  // flush Serial buffer
  flushBuffer(20);
  
  return success;
}


/**
 * Open TCP connection and send the request prep to the chip
 */
bool esp8266::openTCP(uint16_t req_len){
  bool success = true;
  
  // open TCP
  Serial.println(F("AT+CIPSTART=\"TCP\",\"gettreebox.com\",80"));
  if (!findFromResponse("CONNECT", 5000)){
    debugSpi2Uart->print("TCP fail\n", 0);
    success = false;
  }
  findFromResponse("OK", 1000);

  // flush Serial buffer
  flushBuffer(50);
  if (!success) return false;
  
  // send request prep
  char cmd[15] = "";
  sprintf(cmd, "AT+CIPSEND=%d", req_len);
  Serial.println(cmd);
  if (!findFromResponse(">", 5000)){
    debugSpi2Uart->print("Wifi request fail\n", 0);
    success = false;
  }
  
  // flush Serial buffer
  flushBuffer(50);
  if (!success) return false;
  
  return success;
}


/**
 * Send receipt data to the server
 * user_id = customer user id
 * receipt_id = generated receipt_id
 * data = receipt data
 */
bool esp8266::sendReceipt(char* receipt_id, const uint8_t* data, uint16_t data_len){
  debugSpi2Uart->print("Wifi upload\n", 0);
  
  bool success = true;
  uint16_t receipt_id_len = strlen(receipt_id);
  
  // session info
  // FIXME: sessional information
  const char sid[] = "sid=1bc8721cb51e4e09a8c4fb62651a1058, "; uint16_t sid_len = strlen(sid);
  const char token[] = "token=1124d0a9b4414bacaf7c530cda7a8e64\r\n"; uint16_t token_len = strlen(token);
  
  // constants
  const uint8_t NL_LEN = 2;                 // \r\n
  const uint8_t PRE_POST_BOUNDARY_LEN = 2;  // pre and post --
  const uint8_t POST_STR[] = "POST ";
  const uint8_t POST_STR_LEN = 5;

  // content size
  uint16_t content_len = PRE_POST_BOUNDARY_LEN + http_boundary_len + NL_LEN + content_disposition_receipt_content_len + NL_LEN + data_len + NL_LEN +
    PRE_POST_BOUNDARY_LEN + http_boundary_len + PRE_POST_BOUNDARY_LEN + NL_LEN;
  // to string
  char content_len_str[4] = "";
  sprintf(content_len_str, "%d", content_len);
  
  // total length
  uint16_t total_len = POST_STR_LEN + http_dir_len + http_host_len + http_auth_len + device_uid_len + sid_len + token_len + 
    http_content_len_len + strlen(content_len_str) + NL_LEN + http_content_type_len + http_boundary_len + NL_LEN*2 +
    content_len;
  if (receipt_id_len>0)
    total_len += receipt_id_len + http_dir_sec_len;
  
  // establish TCP connection
  if (!openTCP(total_len)) return false;
  
  // send request header
  // Post
  Serial.write(POST_STR, POST_STR_LEN);
  for (uint8_t j=0; j<http_dir_len; j++){
    char buf = pgm_read_byte_near(http_dir+j);
    Serial.write(buf);
  }
  // Receipt id
  if (receipt_id_len>0){
    Serial.write(receipt_id, receipt_id_len);
    for (uint8_t j=0; j<http_dir_sec_len; j++){
      char buf = pgm_read_byte_near(http_dir_sec+j);
      Serial.write(buf);
    }
  }
  // Host
  for (uint8_t j=0; j<http_host_len; j++){
    char buf = pgm_read_byte_near(http_host+j);
    Serial.write(buf);
  }
  // Authorization
  for (uint8_t j=0; j<http_auth_len; j++){
    char buf = pgm_read_byte_near(http_auth+j);
    Serial.write(buf);
  }
  for (uint8_t j=0; j<device_uid_len; j++){
    char buf = pgm_read_byte_near(device_uid+j);
    Serial.write(buf);
  }
  Serial.write(sid, sid_len);
  Serial.write(token, token_len);
  // Content-Length
  for (uint8_t j=0; j<http_content_len_len; j++){
    char buf = pgm_read_byte_near(http_content_len+j);
    Serial.write(buf);
  }
  Serial.write(content_len_str, strlen(content_len_str));
  Serial.write('\r'); Serial.write('\n');
  // Content-Type
  for (uint8_t j=0; j<http_content_type_len; j++){
    char buf = pgm_read_byte_near(http_content_type+j);
    Serial.write(buf);
  }
  for (uint8_t j=0; j<http_boundary_len; j++){
    char buf = pgm_read_byte_near(http_boundary+j);
    Serial.write(buf);
  }
  Serial.write('\r'); Serial.write('\n');
  // single line break
  Serial.write('\r'); Serial.write('\n');
  
  // send request body (receipt content)
  // boundary
  Serial.write('-'); Serial.write('-');
  for (uint8_t j=0; j<http_boundary_len; j++){
    char buf = pgm_read_byte_near(http_boundary+j);
    Serial.write(buf);
  }
  Serial.write('\r'); Serial.write('\n');
  // Content-Disposition
  for (uint8_t j=0; j<content_disposition_receipt_content_len; j++){
    char buf = pgm_read_byte_near(content_disposition_receipt_content+j);
    Serial.write(buf);
  }
  // line break
  Serial.write('\r'); Serial.write('\n');
  // data content
  Serial.write(data, data_len); Serial.write('\r'); Serial.write('\n');
  
  // last boundary
  Serial.write('-'); Serial.write('-');
  for (uint8_t j=0; j<http_boundary_len; j++){
    char buf = pgm_read_byte_near(http_boundary+j);
    Serial.write(buf);
  }
  Serial.write('-'); Serial.write('-');
  Serial.write('\r'); Serial.write('\n');
  
  // end
  Serial.write('\0');

  // verify server response
  if (!findFromResponse("200 OK", 5000)){
    success = false;
    debugSpi2Uart->print("Wifi upload fail\n", 0);
    
  } else if (receipt_id_len<=0) {
    // Find the beginning of the message body
    // It is marked by \r\n\r\n
    if (findFromResponse("\r\n\r\n", 100)){
      // get total received length
      char rec_len_str[5] = "";
      extractDataUntil(rec_len_str, "\r\n", 100);
      sscanf(rec_len_str, "%x", &receipt_id_len);
      
      // extract receipt data
      for (uint16_t j=0; j<receipt_id_len;){
        int16_t c = Serial.read();
        if (c>=0){
          receipt_id[j++] = (uint8_t)c;
        }
      }
    } else {
      success = false;
      debugSpi2Uart->print("Upload body not found\n", 0);
    }
  }
  
  // flush Serial buffer
  flushBuffer(50);
  
  return success;
}


/**
 * Read the receipt data and store into rx
 * Returns the total length of the receipt. If unsuccessful request, return -1
 * start_index = where to start reading
 */
int16_t esp8266::readReceipt(char* receipt_id, uint32_t start_index, uint8_t* rx){
  debugSpi2Uart->print("Wifi download\n", 0);
  
  int16_t totalReceiptLen = 0;
  uint16_t receipt_id_len = strlen(receipt_id);
  
  // session info
  const char sid[] = "sid=1bc8721cb51e4e09a8c4fb62651a1058, "; uint16_t sid_len = strlen(sid);
  const char token[] = "token=1124d0a9b4414bacaf7c530cda7a8e64\r\n"; uint16_t token_len = strlen(token);
  
  // constants
  const uint8_t NL_LEN = 2;            // \r\n
  const char GET_STR[] = "GET ";
  const uint8_t GET_STR_LEN = 4;
  
  // tally the url encoded request
  char url_enc_req[28] = "";
  sprintf(url_enc_req, "?offset=%lu&length=%u", start_index, CHUNK_LEN);
  uint16_t url_enc_req_len = strlen(url_enc_req);
  
  // total length
  uint16_t total_len = GET_STR_LEN + http_dir_len + receipt_id_len + http_dir_sec_len + url_enc_req_len +
    http_host_len + http_auth_len + device_uid_len + sid_len + token_len + 
    http_content_len_len + 1 + NL_LEN + NL_LEN;
  
  // establish TCP connection
  if (!openTCP(total_len)) 
    return -1;
  
  // send request header
  // GET
  Serial.write(GET_STR, GET_STR_LEN);
  for (uint8_t j=0; j<http_dir_len; j++){
    char buf = pgm_read_byte_near(http_dir+j);
    Serial.write(buf);
  }
  // Receipt id
  Serial.write(receipt_id, receipt_id_len);
  for (uint8_t j=0; j<http_dir_sec_len; j++){
    char buf = pgm_read_byte_near(http_dir_sec+j);
    Serial.write(buf);
  }
  // url encoded parameters
  Serial.write(url_enc_req, url_enc_req_len);
  // Host
  for (uint8_t j=0; j<http_host_len; j++){
    char buf = pgm_read_byte_near(http_host+j);
    Serial.write(buf);
  }
  // Authorization
  for (uint8_t j=0; j<http_auth_len; j++){
    char buf = pgm_read_byte_near(http_auth+j);
    Serial.write(buf);
  }
  for (uint8_t j=0; j<device_uid_len; j++){
    char buf = pgm_read_byte_near(device_uid+j);
    Serial.write(buf);
  }
  Serial.write(sid, sid_len);
  Serial.write(token, token_len);
  // Content-Length
  for (uint8_t j=0; j<http_content_len_len; j++){
    char buf = pgm_read_byte_near(http_content_len+j);
    Serial.write(buf);
  }
  Serial.write('0');
  Serial.write('\r'); Serial.write('\n');
  // single line break
  Serial.write('\r'); Serial.write('\n');

  // end
  Serial.write('\0');
  
  // verify server response
  if (!findFromResponse("200 OK", 5000)){
    totalReceiptLen = -1;
    debugSpi2Uart->print("Wifi download fail\n", 0);
    
  } else {
    // Find the beginning of the message body
    // It is marked by \r\n\r\n
    if (findFromResponse("\r\n\r\n", 100)){
      // get total received length
      char rec_len_str[5] = "";
      extractDataUntil(rec_len_str, "\r\n", 100);
      sscanf(rec_len_str, "%x", &totalReceiptLen);
      
      // extract receipt data
      for (uint16_t j=0; j<totalReceiptLen;){
        int16_t c = Serial.read();
        if (c>=0){
          rx[j++] = (uint8_t)c;
        }
      }
    } else {
      totalReceiptLen = -1;
      debugSpi2Uart->print("Download body not found\n", 0);
    }
  }
      
  // flush Serial buffer
  flushBuffer(50);
  
  return totalReceiptLen;
}


/**
 * Flush buffer for timeout ms
 */
void esp8266::flushBuffer(uint16_t timeout){
  uint32_t start;
  uint32_t now;
  for (start=millis(), now=millis(); Utilities::getElapsedTime(start,now)<timeout; Serial.read(), now=millis());
}


/**
 * Send command to the wifi module and wait for response
 * Find successCrit in the response
 */
bool esp8266::findFromResponse(const char* successCrit, uint16_t timeout){
  uint16_t successCritIndex =0;  // serves as a state machine
  
  // timeout
  uint32_t start;
  uint32_t now;
  
  // start timer and read response
  start= millis();
  do{
    int16_t c = Serial.read();
    if (c>-1){
      if (successCrit[successCritIndex] == (char)c){
        if (++successCritIndex >= strlen(successCrit)){
          return true;
        }
      } else{
        successCritIndex = 0;
      }
    }
      
    now = millis();
  } while (Utilities::getElapsedTime(start,now)<timeout);
  
  return false;
}


/**
 * Extract data from Serial and store them in rx, until either timeout or stopCrit
 * Assume here that rx has the required length
 * Return the number of bytes extracted
 */
uint16_t esp8266::extractDataUntil(char* rx, const char* stopCrit, uint16_t timeout){
  uint16_t numExtract = 0;
  uint16_t stopCritIndex =0;  // serves as a state machine
  
  // timeout
  uint32_t start;
  uint32_t now;
  
  // start timer and read response
  start= millis();
  do{
    int16_t c = Serial.read();
    if (c>-1){
      if (stopCrit[stopCritIndex] == (char)c){
        if (++stopCritIndex >= strlen(stopCrit)){
          return true;
        }
      } else{
        stopCritIndex = 0;
        
        // save to rx
        rx[numExtract++] = (char)c;
      }
    }
      
    now = millis();
  } while (Utilities::getElapsedTime(start,now)<timeout);
  
  return numExtract;
}


/**
 * Output the entire response to the debug output
 */
void esp8266::outputRespToDebug(){
  const uint16_t chunk = 700;
  uint16_t timeout = 5000;
  char p[chunk]; memset(p, 0x00, chunk);
   
  // timeout
  uint32_t start;
  uint32_t now;
  
  // data counter
  uint16_t i = 0;
  start = millis();
  do {
    while (Serial.available()>0 && i<chunk-1){
      uint8_t res = Serial.read();
      p[i++] = (char)res;
    }
    
    now = millis();
  } while (Utilities::getElapsedTime(start,now)<timeout && i<chunk-1);
  debugSpi2Uart->print(p, 0);
}
