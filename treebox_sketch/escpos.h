#ifndef _ESC_POS
#define _ESC_POS

#include "SC16IS740.h"

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class escpos {
  public:
    /**
     * debugSpi2Uart = spi2uart object for debug outputting
     * pass_thru_buffer = actual receipt content buffer
     * pass_thru_buffer_max_len = max length of the buffer
     */
    escpos(SC16IS740* debugSpi2Uart, uint8_t* pass_thru_buffer, uint16_t pass_thru_buffer_max_len);

    /**
     * Initialize the bi-directional communication
     * Return true on success
     * 
     * printerSSEL = pin for printer SPI comm select
     * pcSSEL = pin for PC SPI comm select
     */
    bool init(uint8_t printerSSEL, uint8_t pcSSEL);

    
    // The following functions are used to communicate with the printer
    // BEGIN
    
    /**
     * Get status of the printer 
     */
    bool printer_status(uint16_t timeout);
    
    /**
     * Transfer texts or graphics to the printer 
     * 
     * Return true on successful transfer
     */
    bool printer_transfer(const uint8_t *tx, uint16_t tx_len, uint16_t timeout);
    
    // END printer specific functions
    
    
    // The following functions are used to communicate with the PC
    // BEGIN
    
    /**
     * Respond to commands from PC
     * Will store receipt data into pass_thru_buffer
     * 
     * Return: length of receipt content retrieved
     * length return of 0 would mean it's a successful exit (no more content retrieved)
     * -1 on error
     */
    int16_t pc_communicate();

    /**
     * Clear the PC communication state machine
     * Reset the communication buffers and status
     */
    void pc_comm_reset();
    
    // END pc specific functions

  private:
    // command state
    // This enum tells us which command it's in
    enum COMMAND_STATE {
      REALTIME,   // realtime command, requires tx
      RECOGNIZED, // recognized command, but still requires further data for deciphering
      UNDECIDED,  // not sure yet, more info (bytes) required
      PASS_THRU,  // pass buffer to the output + certain ensuing bytes to the output
      IGNORE,     // throw away the buffer
    } pc_cmd_state;
    
    // uart objects
    SC16IS740* printerSpi2Uart;
    SC16IS740* pcSpi2Uart;
    SC16IS740* debugSpi2Uart;

    // Denotes whether the handshake with the PC has been established
    // In escpos, the PC sends status commands and requires the device to respond
    bool pc_initialized;
    
    // This is the buffer used to store the actual receipt content from PC
    // I.e. this is the chunk size
    uint8_t* pass_thru_buffer;
    uint16_t pass_thru_buffer_max_len;
    
    // This is the buffer for received PC command
    static const uint8_t MAX_PC_CMD_BUFFER_LEN = 20;
    uint8_t pc_cmd_buffer[MAX_PC_CMD_BUFFER_LEN];
    uint8_t pc_cmd_buffer_len;
    // These are temporary rx and tx data holders
    static const uint8_t MAX_PC_RX_LEN = 1;     // this must be <= MAX_PC_CMD_BUFFER_LEN
    uint8_t pc_rx_data_len;
    uint8_t pc_rx_data[MAX_PC_RX_LEN];
    
    // Additional bytes to pass through from PC to printer/content server
    uint32_t additional_bytes_passthru;
    
    /**
     * This is the comm independent function for executing a duplex write/read operation (transmit data, then wait for response)
     * This function assumes that the port comm is already opened!
     * Returns the number of bytes read as response
     */
    uint16_t printer_duplex_transfer(const uint8_t *tx, uint16_t txSize, uint8_t *rx, uint16_t maxRxSize, uint16_t timeout);
    
    /**
     * Decipher the command that's stored in the pc_cmd_buffer
     * Generate the tx response if appropriate, the number of bytes stored in tx_len
     * If appropriate, it will also store the bytes to pass through in addtional_bytes_passthru. These are on top of the bytes already in the cmd_buffer
     * Return the command state
     */
    enum COMMAND_STATE decipher_command(uint8_t *tx, uint16_t *tx_len, uint16_t max_tx_len);

};

#endif
