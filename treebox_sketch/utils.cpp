#include "utils.h"

/**
 * Return the elapsed time since start 
 */
uint32_t Utilities::getElapsedTime(uint32_t start, uint32_t now){
  if (now>=start){
    return now-start;
  } else {
    // rollover occurred
    return LARGEST_ULONG-start+now;
  }
}


/**
 * Convert the 128bit uuid from bytes to string
 * Assume uuid_str is 17 bytes long (last byte for nul)
 */
void Utilities::uuidToString(const uint8_t* uuid, char* uuid_str){
  for (int i=0; i<16; i++){
    sprintf(uuid_str+i, "%x", uuid[i]);
  }
  uuid_str[16] = '\0';
}
