#include "escpos.h"
#include "utils.h"

// ESC/POS command codes
#define DLE 0x10
#define ESC 0x1b
#define FS  0x1c
#define GS  0x1d
#define EOT 0x04
#define ENQ 0x05
#define DC4 0x14
#define TAB 0x09
#define LF  0x10
#define SPACE 0x32


/**
 * Constructor
 */
escpos::escpos(SC16IS740* debugSpi2Uart, uint8_t* pass_thru_buffer, uint16_t pass_thru_buffer_max_len){
  this->debugSpi2Uart = debugSpi2Uart;

  // initialize PC communication states
  this->pass_thru_buffer = pass_thru_buffer;
  this->pass_thru_buffer_max_len = pass_thru_buffer_max_len;
}


/**
 * Initialize the bi-directional communication
 * Return true on success
 */
bool escpos::init(uint8_t printerSSEL, uint8_t pcSSEL){
  // reset buffer and states
  pc_comm_reset();

  // printer comm
  printerSpi2Uart = new SC16IS740(printerSSEL);
  // FIXME: baud rate is printer dependent
  if (!printerSpi2Uart->init(38400, Disabled))
    return false;
  
  // pc comm
  pcSpi2Uart = new SC16IS740(pcSSEL);
  // FIXME: baud rate is printer dependent
  if (!pcSpi2Uart->init(38400, Disabled))
    return false;

  return true;
}


/**
 * This is the comm independent function for executing a duplex write/read operation (transmit data, then wait for response)
 * to the printer
 * This function assumes that the port comm is already opened!
 * Returns the number of bytes read as response
 */
uint16_t escpos::printer_duplex_transfer(const uint8_t *tx, uint16_t txSize, uint8_t *rx, uint16_t maxRxSize, uint16_t timeout){
  uint16_t rxSize = 0, receivedLen = 0;

  // timeout
  uint32_t start = millis();
  uint32_t now = millis();

  // SPI
  if (printerSpi2Uart->print(tx, txSize, timeout) < txSize){
    return 0;
  }

  // get response
  do {
    receivedLen = printerSpi2Uart->read(rx+rxSize, maxRxSize-rxSize);
    rxSize += receivedLen;

    now = millis();
  } while (rxSize < maxRxSize && Utilities::getElapsedTime(start,now)<timeout);

  return rxSize;
}


/**
 * Decipher the command that's stored in the pc_cmd_buffer
 * Generate the tx response if appropriate, the number of bytes stored in tx_len
 * If appropriate, it will also store the bytes to pass through in bytes_to_passthru. These are on top of the bytes already in the pc_cmd_buffer
 * Return the command state
 */
escpos::COMMAND_STATE escpos::decipher_command(uint8_t *tx, uint16_t *tx_len, uint16_t max_tx_len){
  // initialize
  memset(tx, 0x00, max_tx_len);
  *tx_len = 0;
  additional_bytes_passthru = 0;

  if (pc_cmd_buffer[0] == 0x00){
    // if the beginning of the cmd is the NULL char, then ignore
    return IGNORE;
  } else if (pc_cmd_buffer[0]!=GS && pc_cmd_buffer[0]!=DLE && pc_cmd_buffer[0]!=ESC){
    // if the beginning of the cmd is not a special char, then it's just plain text
    if (pc_initialized) {
      return PASS_THRU;
    } else {
      return IGNORE;
    }
  }

  // recognized commands
  if (pc_cmd_buffer_len>=4 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='v' && pc_cmd_buffer[2]=='0' && pc_cmd_buffer[3]==0x00){
    // GS v 0 command, print raster bit image

    debugSpi2Uart->print("GS v 0", 0);

    if (pc_cmd_buffer_len == 8){
      uint8_t xL = pc_cmd_buffer[4], xH = pc_cmd_buffer[5], yL = pc_cmd_buffer[6], yH = pc_cmd_buffer[7];

      additional_bytes_passthru = (uint32_t)(xL+xH*256)*(yL+yH*256);

      /*char out[30] = "";
      sprintf(out, " (pass %d bytes)\n", additional_bytes_passthru);*/
      debugSpi2Uart->print(0, " (pass %d bytes)\n", 30, additional_bytes_passthru);
      return PASS_THRU;
    } else {
      additional_bytes_passthru = 0;
      return RECOGNIZED;
    }

  } /*else if (pc_cmd_buffer_len==15 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='(' && pc_cmd_buffer[2]=='L' && pc_cmd_buffer[5]==0x30 && pc_cmd_buffer[6]==0x71 && pc_cmd_buffer[7]==0x30){
    // GS ( L command, store graphics data in print buffer in column format

    uint8_t xL = pc_cmd_buffer[11], xH = pc_cmd_buffer[12], yL = pc_cmd_buffer[13], yH = pc_cmd_buffer[14];
    additional_bytes_passthru = (xL + xH*256)*((uint32_t)(((yL + yH*256) + 7)/8));

    return PASS_THRU;

  } else if (pc_cmd_buffer_len==15 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='(' && pc_cmd_buffer[2]=='L' && pc_cmd_buffer[5]==0x30 && pc_cmd_buffer[6]==0x70){
    // GS ( L command, store graphics data in print buffer in raster format

    uint8_t xL = pc_cmd_buffer[11], xH = pc_cmd_buffer[12], yL = pc_cmd_buffer[13], yH = pc_cmd_buffer[14];
    additional_bytes_passthru = ((uint32_t)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (pc_cmd_buffer_len==16 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='8' && pc_cmd_buffer[2]=='L' && pc_cmd_buffer[7]==0x30 && pc_cmd_buffer[8]==0x71 && pc_cmd_buffer[9]==0x30){
    // GS 8 L command, store graphics data in print buffer in column format

    uint8_t xL = pc_cmd_buffer[13], xH = pc_cmd_buffer[14], yL = pc_cmd_buffer[15], yH = pc_cmd_buffer[16];
    additional_bytes_passthru = ((uint32_t)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (pc_cmd_buffer_len==16 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='8' && pc_cmd_buffer[2]=='L' && pc_cmd_buffer[7]==0x30 && pc_cmd_buffer[8]==0x70){
    // GS 8 L command, store graphics data in print buffer in raster format

    uint8_t xL = pc_cmd_buffer[13], xH = pc_cmd_buffer[14], yL = pc_cmd_buffer[15], yH = pc_cmd_buffer[16];
    additional_bytes_passthru = ((uint32_t)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (pc_cmd_buffer_len==8 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='(' && pc_cmd_buffer[2]=='k'){
    // GS ( k command, store QR code data

    uint8_t pL = pc_cmd_buffer[3], pH = pc_cmd_buffer[4];
    additional_bytes_passthru = (pL+pH*256)-3;

    return PASS_THRU;
  } else if (pc_cmd_buffer_len==4 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='k'){
    // GS k command, print barcode
    // Assumes function B encoding

    additional_bytes_passthru = pc_cmd_buffer[3];

    return PASS_THRU;
  } */else if (pc_cmd_buffer_len==3 && pc_cmd_buffer[0]==ESC && pc_cmd_buffer[1]=='=' && pc_cmd_buffer[2]==0x01){
    // ESC = command, select peripheral device

    debugSpi2Uart->print("ESC =\n", 0);

    pc_initialized = true;
    return IGNORE;

  } else if (pc_cmd_buffer_len==3 && pc_cmd_buffer[0]==DLE && pc_cmd_buffer[1]==EOT){
    // DLE EOT command

    debugSpi2Uart->print("DLE EOT\n", 0);

    if (pc_cmd_buffer[2]==0x01){
      tx[0] = 0x16;
      *tx_len = 1;
    } else if (pc_cmd_buffer[2]==0x02 || pc_cmd_buffer[2]==0x03 || pc_cmd_buffer[2]==0x04){
      tx[0] = 0x12;
      *tx_len = 1;
    }

    pc_initialized = true;
    return REALTIME;

  } else if (pc_cmd_buffer_len==3 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='I'){
    // GS I command, transmit printer ID

    debugSpi2Uart->print("GS I\n", 0);

    if (pc_cmd_buffer[2] == 0x01){
      // printer model
      tx[0] = 0x20;
      *tx_len = 1;
    } else if (pc_cmd_buffer[2] == 0x43){
      // printer ID
      tx[0] = '_';
      tx[1] = 'T';
      tx[2] = 'M';
      tx[3] = '-';
      tx[4] = 'T';
      tx[5] = '8';
      tx[6] = '8';
      tx[7] = 'I';
      tx[8] = 'I';
      tx[9] = 'I';
      tx[10] = '\0';
      *tx_len = 11;
    } else if (pc_cmd_buffer[2] == 0x45){
      // printer information B
      tx[0] = 0x5F;
      tx[1] = 0x00;
      *tx_len = 2;
    }

    pc_initialized = true;
    return REALTIME;

  } else if (pc_cmd_buffer_len==3 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='a'){
    // GS a, basic ASB configuration

    debugSpi2Uart->print("GS a\n", 0);

    tx[0] = 0x14;
    tx[1] = 0x00;
    tx[2] = 0x00;
    tx[3] = 0x0F;
    *tx_len = 4;

    pc_initialized = true;
    return REALTIME;

  } else if (pc_cmd_buffer_len==4 && pc_cmd_buffer[0]==DLE && pc_cmd_buffer[1]==DC4 && pc_cmd_buffer[2]==0x07){
    // DLE DC4 fn=7, transmit specified status in real time

    debugSpi2Uart->print("DLE DC4 0x07\n", 0);

    pc_initialized = true;
    if (pc_cmd_buffer[3]==0x01){
      tx[0] = 0x14;
      tx[1] = 0x00;
      tx[2] = 0x00;
      tx[3] = 0x0F;
      *tx_len = 4;

      return REALTIME;
    } else {
      return IGNORE;
    }

  } else if (pc_cmd_buffer_len==3 && pc_cmd_buffer[0]==GS && pc_cmd_buffer[1]=='r'){
    // GS r, transmit status

    debugSpi2Uart->print("GS r\n", 0);
    if (pc_cmd_buffer[2]==0x01){
      // paper sensor status
      tx[0] = 0x00;
      *tx_len = 1;
    } else if (pc_cmd_buffer[2]==0x02){
      // drawer kickout
      tx[0] = 0x00;
      *tx_len = 1;
    } else if (pc_cmd_buffer[2]==0x04){
      // ink status
      tx[0] = 0x00;
      *tx_len = 1;
    }

    pc_initialized = true;
    return REALTIME;

  } else {
    return UNDECIDED;
  }
}


/**
 * Clear the PC communication state machine
 * Reset the communication buffers and status
 */
void escpos::pc_comm_reset(){
  // reset the pc_cmd_buffer and pc_cmd_state
  memset(pc_cmd_buffer, 0x00, MAX_PC_CMD_BUFFER_LEN);
  pc_cmd_buffer_len = 0;
  pc_cmd_state = UNDECIDED;

  // length of actual stored receipt data
  uint16_t pass_thru_buffer_len = 0;

  // clear rx data and the handshake
  memset(pc_rx_data, 0x00, MAX_PC_RX_LEN);
  pc_rx_data_len = 0;
  pc_initialized = false;
}


/**
 * Respond to commands from PC
 * Will store receipt data into pass_thru_buffer
 * 
 * Return: length of receipt content retrieved
 * length return of 0 would mean it's a successful exit (no more content retrieved)
 * -1 on error
 */
int16_t escpos::pc_communicate(){
  // length of actual stored receipt data
  uint16_t pass_thru_buffer_len = 0;

  // set timeout for when no data arrives
  uint16_t timeout = 500;
  uint16_t elapsedTime = 0;
  uint32_t start;
  uint32_t now;
  start = millis();

  // before we go into the loop, let's find out if we need to send the pc_cmd_buffer to pass_thru_buffer
  if (pc_cmd_state == PASS_THRU){
    memcpy(pass_thru_buffer+pass_thru_buffer_len, pc_cmd_buffer, pc_cmd_buffer_len);
    pass_thru_buffer_len += pc_cmd_buffer_len;
  }

  // afterward, should we move the pc_rx_data to pc_cmd_buffer or pass_thru_buffer?
  if (pc_rx_data_len > 0){
    if (additional_bytes_passthru > 0){
      memcpy(pass_thru_buffer+pass_thru_buffer_len, pc_rx_data, pc_rx_data_len);
      pass_thru_buffer_len += pc_rx_data_len;
      additional_bytes_passthru -= pc_rx_data_len;
    } else {
      memcpy(pc_cmd_buffer, pc_rx_data, pc_rx_data_len);
      pc_cmd_buffer_len += pc_rx_data_len;
    }
  }

  // resaet pc_rx_data
  memset(pc_rx_data, 0x00, MAX_PC_RX_LEN);
  pc_rx_data_len = 0;

  // This loop will block forever unless there is no more incoming RX or if pass_thru_buffer is full
  do {
    // read from spi2uart bridge
    pc_rx_data_len = pcSpi2Uart->read(pc_rx_data, MAX_PC_RX_LEN);

    // If nothing is received, begin timeout countdown
    if (pc_rx_data_len > 0) {

      if (additional_bytes_passthru > 0) {
        // break from the do loop if passthru_buffer overflows
        if (pass_thru_buffer_len + pc_rx_data_len > pass_thru_buffer_max_len){
          goto END_OF_DO;
        }
        
        // straight pass through to the output
        memcpy(pass_thru_buffer+pass_thru_buffer_len, pc_rx_data, pc_rx_data_len);
        pass_thru_buffer_len += pc_rx_data_len;
        
        additional_bytes_passthru -= pc_rx_data_len;

      } else {
        // If a special command char arrives and is preceded by another command that is marked as UNDECIDED, then
        // we should send the previous command
        // Also,
        // Check if pc_cmd_buffer overflows, if so, then
        // we have no choice but to pass the original content to outData
        if ((pc_cmd_buffer_len>0 && pc_cmd_state==UNDECIDED && (pc_rx_data[0]==DLE || pc_rx_data[0]==ESC || pc_rx_data[0]==GS))
          || (pc_cmd_buffer_len + pc_rx_data_len) > MAX_PC_CMD_BUFFER_LEN){

          // break from the do loop if passthru_buffer overflows
          if (pass_thru_buffer_len + pc_cmd_buffer_len > pass_thru_buffer_max_len){
            goto END_OF_DO;
          }

          // pass everything to outData
          memcpy(pass_thru_buffer+pass_thru_buffer_len, pc_cmd_buffer, pc_cmd_buffer_len);
          pass_thru_buffer_len += pc_cmd_buffer_len;

          // reset the pc_cmd_buffer
          memset(pc_cmd_buffer, 0x00, MAX_PC_CMD_BUFFER_LEN);
          pc_cmd_buffer_len = 0;
        }

        // Copy received data to pc_cmd_buffer
        memcpy(pc_cmd_buffer+pc_cmd_buffer_len, pc_rx_data, pc_rx_data_len);
        pc_cmd_buffer_len += pc_rx_data_len;
        pc_rx_data_len = 0; memset(pc_rx_data, 0x00, MAX_PC_RX_LEN);
        

        // decipher the command
        const uint8_t max_tx_len = 20;
        uint8_t txData[max_tx_len]; memset(txData, 0x00, max_tx_len);
        uint16_t tx_len = 0;
        pc_cmd_state = decipher_command(txData, &tx_len, max_tx_len);

        switch (pc_cmd_state){
        case PASS_THRU:
          // break from the do loop if passthru_buffer overflows
          if (pass_thru_buffer_len + pc_cmd_buffer_len > pass_thru_buffer_max_len){
            goto END_OF_DO;
          }
        
          // transfer the buffer content to outData
          memcpy(pass_thru_buffer+pass_thru_buffer_len, pc_cmd_buffer, pc_cmd_buffer_len);
          pass_thru_buffer_len += pc_cmd_buffer_len;

          // reset the pc_cmd_buffer
          memset(pc_cmd_buffer, 0x00, MAX_PC_CMD_BUFFER_LEN);
          pc_cmd_buffer_len = 0;
          // reset the cmd_state
          pc_cmd_state = UNDECIDED;

          break;

        case REALTIME:
          // requires a tx response
          if (pcSpi2Uart->print(txData, tx_len, timeout) < tx_len){
            return -1;
          }

          // reset the pc_cmd_buffer
          memset(pc_cmd_buffer, 0x00, MAX_PC_CMD_BUFFER_LEN);
          pc_cmd_buffer_len = 0;
          // reset the cmd_state
          pc_cmd_state = UNDECIDED;

          break;

        case IGNORE:
          // throw away the buffer content
          memset(pc_cmd_buffer, 0x00, MAX_PC_CMD_BUFFER_LEN);
          pc_cmd_buffer_len = 0;
          // reset the cmd_state
          pc_cmd_state = UNDECIDED;

          break;
        }
      }

      // restart the clock
      start = millis();
    }

    // advance the clock
    now = millis();
  } while (Utilities::getElapsedTime(start,now)<timeout);

  // label for goto statement, in order to get out of the do loop
  END_OF_DO:

  return pass_thru_buffer_len;
}


/**
 * Get status of the printer 
 */
bool escpos::printer_status(uint16_t timeout){
  const uint16_t MAX_TX_SIZE = 10; uint8_t tx[MAX_TX_SIZE]; memset(tx, 0x00, MAX_TX_SIZE);
  uint16_t txSize = 0;
  const uint16_t MAX_RX_SIZE = 10; uint8_t rx[MAX_RX_SIZE]; memset(rx, 0x00, MAX_RX_SIZE);
  uint16_t rxSize = 0;

  // DLE EOT printer status
  tx[0] = DLE;
  tx[1] = EOT;
  tx[2] = 0x01;
  txSize = 3;
  // write and get response
  rxSize = printer_duplex_transfer(tx, txSize, rx, 1, timeout);
  // decipher response
  if (rxSize < 1 || rx[0] != 0x16){
    return false;
  }

  // GS a ASB status
  tx[0] = GS;
  tx[1] = 'a';
  tx[2] = 0xFF;
  txSize = 3;
  // write and get response
  rxSize = printer_duplex_transfer(tx, txSize, rx, 4, timeout);
  // decipher response
  if (rxSize < 4 || rx[0] != 0x14 || rx[1] != 0x00 || rx[2] & 0x0C != 0x00){
    return false;
  }

  return true;
}


/**
 * Transfer texts or graphics to the printer 
 * 
 * Return true on sucessful transfer
 */
bool escpos::printer_transfer(const uint8_t *data, uint16_t data_len, uint16_t timeout){
  // select printer command
#define PRE_TX_LEN 3
  uint8_t pre_tx[PRE_TX_LEN] = {ESC, '=', 0x01}; 

  // transmit
  if (printerSpi2Uart->print(pre_tx, PRE_TX_LEN, timeout) < PRE_TX_LEN)
    return false;
  if (printerSpi2Uart->print(data, data_len, timeout) < data_len)
    return false;

  return true;
}
