#include "SC16IS740.h"
#include <SPI.h>


/* Baud rate */
#define SC16IS750_XTAL_FREQ              14745600UL /* On-board crystal (New mid-2010 Version) */
#define SC16IS750_PRESCALER_1                   1   /* Default prescaler after reset           */
#define SC16IS750_PRESCALER_4                   4   /* Selectable by setting MCR[7]            */
#define SC16IS750_PRESCALER                      SC16IS750_PRESCALER_1  
#define SC16IS750_BAUDRATE_DIVISOR(baud)       ((SC16IS750_XTAL_FREQ/SC16IS750_PRESCALER)/(baud*16UL))


/** IO Control register */
#define IOC_SW_RST			(0x04)

/** See section 8.10 of the datasheet for definitions
 * of bits in the Enhanced Features Register (EFR)
 */
#define EFR_ENABLE_CTS                  (1 << 7)
#define EFR_ENABLE_RTS                  (1 << 6)
#define EFR_ENABLE_XOFF2_CHAR_DETECT    (1 << 5)
#define EFR_ENABLE_ENHANCED_FUNCTIONS   (1 << 4)
// EFR[3:0] are used to define Software Flow Control mode
// See section 7.3
#define EFR_DISABLE_TX_FLOW_CTRL        (0x0 << 2)
#define EFR_TX_XON2_XOFF2               (0x1 << 2)
#define EFR_TX_XON1_XOFF1               (0x2 << 2)
#define EFR_TX_XON2_1_XOFF2_1           (0x3 << 2)
 
#define EFR_DISABLE_RX_FLOW_CTRL        (0x0 << 0)
#define EFR_RX_XON2_XOFF2               (0x1 << 0)
#define EFR_RX_XON1_XOFF1               (0x2 << 0)
#define EFR_RX_XON2_1_XOFF2_1           (0x3 << 0)
 
#define EFR_TX_XON2_XOFF2_RX_FLOW       (0x1 << 2) | (0x3 << 0)
#define EFR_TX_XON1_XOFF1_RX_FLOW       (0x2 << 2) | (0x3 << 0)
#define EFR_TX_XON2_1_XOFF2_1_RX_FLOW   (0x3 << 2) | (0x3 << 0)


/** See section 8.4 of the datasheet for definitions
  * of bits in the Line Control Register (LCR)
  */
#define LCR_BITS5                      0x00
#define LCR_BITS6                      0x01
#define LCR_BITS7                      0x02
#define LCR_BITS8                      0x03
 
#define LCR_STOP_BITS1                 0x00
#define LCR_STOP_BITS2                 0x04
 
#define LCR_NONE                       0x00
#define LCR_ODD                        0x08
#define LCR_EVEN                       0x18
#define LCR_FORCED1                    0x28
#define LCR_FORCED0                    0x38
 
#define LCR_BRK_ENA                    0x40
#define LCR_BRK_DIS                    0x00
 
#define LCR_ENABLE_DIV                 0x80
#define LCR_DISABLE_DIV                0x00
 
#define LCR_ENABLE_ENHANCED_FUNCTIONS (0xBF)


/** See section 8.3 of the datasheet for definitions
  * of bits in the FIFO Control Register (FCR)
  */
#define FCR_RX_IRQ_60                 (3 << 6)
#define FCR_RX_IRQ_56                 (2 << 6)
#define FCR_RX_IRQ_16                 (1 << 6)
#define FCR_RX_IRQ_8                  (0 << 6)
//TX Level only accessible when EFR[4] is set
#define FCR_TX_IRQ_56                 (3 << 4)
#define FCR_TX_IRQ_32                 (2 << 4)
#define FCR_TX_IRQ_16                 (1 << 4)
#define FCR_TX_IRQ_8                  (0 << 4)
//#define FCR_RESERVED                  (1 << 3)
#define FCR_TX_FIFO_RST               (1 << 2)
#define FCR_RX_FIFO_RST               (1 << 1)
#define FCR_ENABLE_FIFO               (1 << 0)


/** See section 8.5 of the datasheet for definitions
  * of bits in the Line status register (LSR)
  */
#define LSR_DR   (0x01) /* Data ready in RX FIFO                       */
#define LSR_OE   (0x02) /* Overrun error                               */
#define LSR_PE   (0x04) /* Parity error                                */
#define LSR_FE   (0x08) /* Framing error                               */
#define LSR_BI   (0x10) /* Break interrupt                             */
#define LSR_THRE (0x20) /* Transmitter holding register (FIFO empty)   */
#define LSR_TEMT (0x40) /* Transmitter empty (FIFO and TSR both empty) */
#define LSR_FFE  (0x80) /* At least one PE, FE or BI in FIFO           */


/* The maximum length of block write available on the THR register (64 bytes) */
#define BULK_BLOCK_LEN              16
#define POLLING_DELAY               1

// spi settings
static SPISettings spiSettingSpi2Uart(8000000, MSBFIRST, SPI_MODE0);


SC16IS740::SC16IS740(uint8_t sselPin){
  spiPort = sselPin;
  
  // set SSEL active low
  digitalWrite(spiPort, HIGH);
}


/**
 * Read from a register
 * A register is a 8-bit storage element on the SC16IS750 chip
 * Return the char read
 */
uint8_t SC16IS740::read_register(enum RegisterName registerAddress){
  // Used in SPI read operations to flush slave's shift register

  // tx buffer
  uint8_t tx[2];
  tx[0] = registerAddress | 0x80;  // 0x80 is required to access read function
  tx[1] = 0xff;
  // rx buffer
  uint8_t rx = 0x00;

  // perform duplex write operation
  SPI.beginTransaction(spiSettingSpi2Uart);
  digitalWrite(spiPort, LOW);
  SPI.transfer(tx[0]);
  rx = SPI.transfer(tx[1]);
  digitalWrite(spiPort, HIGH);
  SPI.endTransaction();
  return rx;
}


/**
 * Write to a register
 * A register is a 8-bit storage element on the SC16IS750 chip
 */
void SC16IS740::write_register(enum RegisterName registerAddress, uint8_t data){
  // tx buffer
  uint8_t tx[2];
  tx[0] = registerAddress; tx[1] = data;

  // write
  SPI.beginTransaction(spiSettingSpi2Uart);
  digitalWrite(spiPort, LOW);
  SPI.transfer(tx[0]);
  SPI.transfer(tx[1]);
  digitalWrite(spiPort, HIGH);
  SPI.endTransaction();
}


/**
 * Write a block of data to the THR register
 * Assumes that THR has enough space to write
 */
void SC16IS740::write_block(const uint8_t* data, const uint16_t len){
  // tx buffer
  enum RegisterName reg = THR;
  
  SPI.beginTransaction(spiSettingSpi2Uart);
  digitalWrite(spiPort, LOW);

  // write
  for (int i=0; i<len+1; i++){
    if (i==0)
      SPI.transfer(reg);
    else
      SPI.transfer(data[i-1]);
  }
  
  digitalWrite(spiPort, HIGH);
  SPI.endTransaction();
}


/**
 * Set up baud rate
 */
void SC16IS740::set_baud(uint32_t baudrate){
  unsigned long divisor = SC16IS750_BAUDRATE_DIVISOR(baudrate);
  char lcr_tmp = 0x00;

  lcr_tmp = read_register(LCR);                            // Read current LCR register
  write_register(LCR, lcr_tmp | LCR_ENABLE_DIV);           // Enable Divisor registers
  write_register(DLL, (divisor & 0xFF));            //   write divisor LSB
  write_register(DLH, ((divisor >> 8) & 0xFF));            //   write divisor MSB
  write_register(LCR, lcr_tmp);                            // Restore LCR register, activate regular RBR, THR and IER registers
}


/**
 * Set up flow control 
 */
void SC16IS740::set_flow_control(enum Flow type){
  char lcr_tmp = 0x00, efr_tmp = 0x00;
  
  // We need to enable flow control to prevent overflow of buffers and lose data.
 
  switch (type) {
    case Disabled: 
      break;
    case RTS:
      efr_tmp = EFR_ENABLE_RTS;
      break;     
    case CTS:
      efr_tmp = EFR_ENABLE_CTS;
      break;     
    case RTSCTS:
      efr_tmp = EFR_ENABLE_RTS | EFR_ENABLE_CTS;
      break;
    case XONXOFF:
      efr_tmp = EFR_TX_XON1_XOFF1 | EFR_RX_XON1_XOFF1;
      break;
  }

  // read and save LCR register
  lcr_tmp = read_register(LCR);
  // write magic number 0xBF to enable access to EFR register
  write_register(LCR, LCR_ENABLE_ENHANCED_FUNCTIONS);
  // set flow and enable enhanced functions
  write_register(EFR, efr_tmp);
  // set XON1, XOFF1
  write_register(XON1, 0x11); // DC1 control char
  write_register(XOFF1, 0x13); // DC3 control char
  // restore LCR register
  write_register(LCR, lcr_tmp);
}


/**
 * Set up line control
 */
void SC16IS740::set_line_control(uint8_t parity, uint8_t stopbits, uint8_t numbits){
  write_register(LCR, parity | stopbits | numbits);
}


/**
 * Reset
 * Active low 
 */
void SC16IS740::reset(){
  write_register(IOCTRL, IOC_SW_RST);
}


/**
 * Set FIFO control (first-in first-out) 
 */
void SC16IS740::set_fifo_control(bool enable){
  // reset TXFIFO, reset RXFIFO, non FIFO mode  
  write_register(FCR, FCR_TX_FIFO_RST | FCR_RX_FIFO_RST);

  char fifoformat = FCR_RX_IRQ_8 | FCR_TX_IRQ_8;  //Default
  if (enable)
    // enable FIFO mode and set FIFO control values
    write_register(FCR, fifoformat | FCR_ENABLE_FIFO);
  else
    // disable FIFO mode and set FIFO control values  
    write_register(FCR, fifoformat);
 
  // Set Trigger level register TLR for RX and TX interrupt generation
  // Note TLR only accessible when EFR[4] is set (enhanced functions enable) and MCR[2] is set
  //   TRL Trigger levels for RX and TX are 0..15, meaning 0-60 with a granularity of 4 chars    
  // When TLR for RX or TX are 'Zero' the corresponding values in FCR are used. The FCR settings
  // have less resolution (only 4 levels) so TLR is considered an enhanced function.
  write_register(TLR, 0x00);                                     // Use FCR Levels
}


/**
 * Test uart connection
 * Return 1 if connectd, 0 otherwise
 */
bool SC16IS740::connected(){
  // Perform read/write test on the SPR register (scratch pad) to check if UART is working
  const char TEST_CHARACTER = 'Q';
  write_register(SPR, TEST_CHARACTER);
  char a = read_register(SPR);
  return (a == TEST_CHARACTER);
}


/**
 * Test if there is data to be read on the uart bridge 
 * Return 1 if there is, 0 otherwise
 */
bool SC16IS740::readable(){
  /*if (read_register(LSR) & LSR_DR) { // Data in Receiver Bit, at least one character waiting
    return 1;
  }*/

  uint8_t rxlvl = read_register(RXLVL);

  if (rxlvl > 0)
    return 1;
  else
    return 0;
}


/**
 * Initialize the board 
 * SPI = 0 or 1
 * Return true on success, false on failure
 */
bool SC16IS740::init(uint32_t baud, enum Flow flow_ctrl){
  if (!spiInitialized){
    // initialize the spi2uart chip
    reset();
    set_baud(baud);
    set_flow_control(flow_ctrl);
    set_fifo_control(1);
    set_line_control(LCR_EVEN, LCR_STOP_BITS1, LCR_BITS8);
  }

  // test connection
  if (connected()){
    spiInitialized = true;
    return true;
  } else {
    spiInitialized = false;
    return false;
  }
}


/**
 * End the spi2uart and return settings to default
 */
void SC16IS740::end(){
  spiInitialized = false;
}


/**
 * Transfer data to uart
 * Will block until the len amount of bytes have been transferred, unless
 * connection has been severed, or idle time has surpassed TIMEOUT
 * Return the total number of bytes transferred
 */
uint16_t SC16IS740::print(const uint8_t* data, const uint16_t len, const uint16_t timeout){
  int totalDelay = 0, res = 0;
  uint16_t len_w = len;
  
  // Write blocks of BULK_BLOCK_LEN  
  while (len_w > 0) {
    while (read_register(TXLVL) < BULK_BLOCK_LEN) {
      // Wait for space in TX buffer
      delay(2);
      totalDelay+=2;

      if (totalDelay >= timeout)
        return res;
    };

    // determine how many bytes to bulk write
    int writeSize = (len_w >= BULK_BLOCK_LEN) ? BULK_BLOCK_LEN : len_w;
    write_block(data, writeSize);    
    res += writeSize;

    if (len_w >= BULK_BLOCK_LEN){
      len_w  -= BULK_BLOCK_LEN;
      data += BULK_BLOCK_LEN;
    } else
      break;
  }

  return res;
}


/**
 * Transfer string to uart
 * Will block until the len amount of bytes have been transferred, unless
 * connection has been severed, or idle time has surpassed TIMEOUT
 * Return the total number of bytes transferred
 */
uint16_t SC16IS740::print(const char* tx, const uint16_t timeout){
  print((uint8_t *)tx, strlen(tx), timeout);
}


/**
 * Transfer format string followed by arguments to uart
 * The arguments and formatting are as to be used with sprintf or std c printf
 */
uint16_t SC16IS740::print(const uint16_t timeout, const char* format, uint16_t max_tx_len, ...){
  char buf[max_tx_len];
  va_list args;
  va_start(args, max_tx_len);
  vsnprintf(buf, max_tx_len, format, args);
  va_end(args);

  print(buf, timeout);
}


/**
 * Read data from uart
 * Will block until len bytes have been read or no more bytes to read
 * Return the number of bytes read
 */
uint16_t SC16IS740::read(uint8_t* res, const uint16_t len){
  if (!readable())
    return 0;

  int count = 0;
  // RHR = Receive Holding Register
  while (readable() && count < len){
    res[count] = read_register(RHR);
    count++;
  }

  return count;
}
