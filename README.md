# POS Transceiver Device Program (Pilot 1) #


## Introduction ##

This is the program for Pilot 1 of the TreeBox POS transceiver device.  
It has been tested with EPSON TM-T88III. The program is targeted for an Arduino with ATmega328P micro-controller.


## Pre-requisites ##

1. For ESP8266, set AT+CWMODE=3 prior to connecting to Arduino